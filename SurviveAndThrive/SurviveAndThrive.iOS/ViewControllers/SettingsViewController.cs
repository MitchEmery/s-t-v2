﻿using System;
using Foundation;
using UIKit;
using System.CodeDom.Compiler;
using SafariServices;
namespace SurviveAndThrive.iOS
{
    public partial class SettingsViewController : UIViewController
    {
        //settings for the toggle switches
        public bool TrafalgarFilter { get; set; }
        public bool HMCFilter { get; set; }
        public bool DavisFilter { get; set; }

        //the manager we will use for updating the realm
        private RealmManager _rManager;

        public SettingsViewController(IntPtr handle) : base(handle)
        {
            //intialize the realm manager
            _rManager = new RealmManager();
        }

        public override void ViewDidLoad()
        {
            //Load the existing defaults, passed in from the home view controller
			base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.
            //load the user defaults, from the realm
            TrafalgarFilter = TrafalgarToggle.On = _rManager.GetUser().TrafalgarPreference;
            DavisFilter = DavisToggle.On = _rManager.GetUser().DavisPreference;
            HMCFilter = HMCToggle.On = _rManager.GetUser().HMCPreference;


            _completionTracker.Layer.CornerRadius = 10f;
            UpdateCompletionTracker();

        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }

        /// <summary>
        /// Update user pref on toggle state change
        /// </summary>
        /// <param name="sender">Sender.</param>
        partial void TrafToggleValueChange(UISwitch sender)
        {
            TrafalgarFilter = (sender as UISwitch).On;

            _rManager.UpdateUserPrefs(TrafalgarFilter, HMCFilter, DavisFilter);
        }

		/// <summary>
		/// Update user pref on toggle state change
		/// </summary>
		/// <param name="sender">Sender.</param>
		partial void DavisToggleValueChange(UISwitch sender)
        {
            DavisFilter = (sender as UISwitch).On;

            _rManager.UpdateUserPrefs(TrafalgarFilter, HMCFilter, DavisFilter);
        }

		/// <summary>
		/// Update user pref on toggle state change
		/// </summary>
		/// <param name="sender">Sender.</param>
		partial void HMCToggleValueChanged(UISwitch sender)
        {
            HMCFilter = (sender as UISwitch).On;

            _rManager.UpdateUserPrefs(TrafalgarFilter, HMCFilter, DavisFilter);
        }

		[Action("UnwindFromCreditsDetail:")]
		public void UnwindFromCreditsDetail(UIStoryboardSegue sender)
		{
		}


        private void UpdateCompletionTracker()
        {
            _completionTracker.SetProgress(_rManager.GetCompletionPercentage(), true);
        }

    }
}

 