﻿using Realms;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;

namespace SurviveAndThrive
{

    public enum Campus : byte
    {
        HMC = 1,
        Davis = 2,
        Trafalgar = 3,
        All = 4
    }

    public class Article : RealmObject
    {
        //The article class repersents the base unit of the Survive and Thrive App
        public string Title { get; set; }
        //the body of the article
        public string Text { get; set; }
        //The campus filter
        public byte _campusFilter { get; set; }
        //The content type for this article
        public string ContentType { get; set; }
        //The bool for controlling favorites
        public bool IsFavorite { get; set; }
        //the corresponding youtube video
        public YoutubeVideo ArticleVideo { get; set; }
		//Has the user completed the article
		public bool IsComplete { get; set; }
		//Challenege specific variables
		private ThreadSafeReference _threadSafeRef;
		//The system.Threading.Timer to handle the challenege duration
		private Timer _challengeTimer;
   
        /// <summary>
        /// The main constructor for the article, setting all the propertys
        /// </summary>
        /// <param name="title">Sets the title of the article</param>
        /// <param name="url">The youtube URL for this article</param>
        /// <param name="text">The </param>
        /// <param name="cmpus">sets the campus for this article</param>
        /// <param name="contentType">Adds this article to the corresponding content group</param>
        public Article(string title, string url, string text, Campus cmpus, string contentType)
        {
            //take the parameters and apply it to the propertys
            Title = title;
            ArticleVideo = new YoutubeVideo(url);
            Text = text;
            _campusFilter = (byte)cmpus;
            ContentType = contentType;
        }

        public Article()
        { }

        public void setCampusFilter(Campus s)
        {
            _campusFilter = (byte)s;
        }
        public Campus getCampusFilter()
        {
            return (Campus)_campusFilter;
        }

        public void UpdateFavorite(bool isFav)
        {
            this.Realm.Write(() => { this.IsFavorite = isFav; });
        }

        public override int GetHashCode()
        {
            return (  _campusFilter.GetHashCode() ^ (Title ?? String.Empty).GetHashCode() ^ (Text ?? String.Empty).GetHashCode() ^ (ContentType ?? String.Empty).GetHashCode() ^ ArticleVideo.VideoID.GetHashCode() );
                                
        }

        public override bool Equals(object obj)
        {

            Article compareeArticle = obj as Article;
            if (obj == null || compareeArticle == null) { return false; }

            

            return _campusFilter == compareeArticle._campusFilter &&
                                                   Title == compareeArticle.Title &&
                                                   Text == compareeArticle.Text &&
                                                   ContentType == compareeArticle.ContentType &&
                                                   ArticleVideo.VideoID == compareeArticle.ArticleVideo.VideoID;
            
        }

        public void StartChallenge()
        {
			//instantiate the thread safe reference
			_threadSafeRef = ThreadSafeReference.Create(this);
			//set the timer 
			TimeSpan ts = new TimeSpan(0, 0, 30);
			_challengeTimer = new Timer(ChallengeCompleted, null, ts, ts);
        }

		public void EndChallenge()
		{
			//stop the timer
            //BUG
			_challengeTimer.Dispose();
		}

		public void ChallengeCompleted(object sender)
		{
			//create a realm for this thread, based on the existing config
			Realm secondRealm = Realm.GetInstance(new RealmConfiguration { ShouldDeleteIfMigrationNeeded = true });
			//Grab the article from the second realm, and resolve the thread safe reference
			Article art = secondRealm.ResolveReference<Article>(_threadSafeRef as ThreadSafeReference.Object<Article>);
			//complete the challenege controlling boolean
			secondRealm.Write(() => { art.IsComplete = true; });
			//end the challenge
			EndChallenge();
		}

	}
}
