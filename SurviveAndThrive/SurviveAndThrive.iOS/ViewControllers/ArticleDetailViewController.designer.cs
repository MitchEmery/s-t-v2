// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace SurviveAndThrive.iOS
{
    [Register ("ArticleDetailViewController")]
    partial class ArticleDetailViewController
    {
        [Outlet]
        UIKit.UIScrollView _articleScrollView { get; set; }

        [Outlet]
        UIKit.UILabel _lblTitle { get; set; }

        [Outlet]
        public UIKit.UITextView _txtScroller { get; private set; }

        [Outlet]
        public UIKit.UIWebView _webViewer { get; private set; }

        [Action ("OnDownFont:")]
        partial void OnDownFont (Foundation.NSObject sender);

        [Action ("OnPinch:")]
        partial void OnPinch (Foundation.NSObject sender);

        [Action ("OnPinchToZoom:")]
        partial void OnPinchToZoom (Foundation.NSObject sender);

        [Action ("OnUpFont:")]
        partial void OnUpFont (Foundation.NSObject sender);

        void ReleaseDesignerOutlets ()
        {
            if (_articleScrollView != null) {
                _articleScrollView.Dispose ();
                _articleScrollView = null;
            }

            if (_lblTitle != null) {
                _lblTitle.Dispose ();
                _lblTitle = null;
            }

            if (_txtScroller != null) {
                _txtScroller.Dispose ();
                _txtScroller = null;
            }

            if (_webViewer != null) {
                _webViewer.Dispose ();
                _webViewer = null;
            }
        }
    }
}