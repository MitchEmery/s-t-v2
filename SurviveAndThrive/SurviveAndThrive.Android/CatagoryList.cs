﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.Lang;

namespace SurviveAndThrive.Droid
{
    [Activity(Label = "ListActivity")]

    public class ViewHolder : Java.Lang.Object
    {
        public TextView txtTitle { get; set; }
        public TextView txtDescription { get; set; }
        public ImageView thumbnailImage { get; set; }
    }

    public class CatagoryList : BaseAdapter
    {
        private Activity activity;
        private List<Article> articles;
        private List<YoutubeVideo> videos;

        public CatagoryList(Activity activity, List<Article> articles, List<YoutubeVideo> videos)
        {
            this.activity = activity;
            this.articles = articles;
            this.videos = videos;
        }

        public override int Count
        {
            get
            {
                return articles.Count;
            }
        }

        public override Java.Lang.Object GetItem(int position)
        {
            return null;
        }

        public override long GetItemId(int position)
        {
            // Id Currently does not exist will have to creat it for the future
            return position;
            //return articles[position].Id;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var view = convertView ?? activity.LayoutInflater.Inflate(Resource.Layout.VideoListRow, parent, false);

            var txtTitle = view.FindViewById<TextView>(Resource.Id.Title);
            var txtTime = view.FindViewById<TextView>(Resource.Id.Time);
            var txtDesc= view.FindViewById<TextView>(Resource.Id.Description);
            var thumbnail = view.FindViewById<ImageView>(Resource.Id.Image);

            txtTitle.Text = articles[position].Title;
            txtTime.Text = videos[position].VideoDuration;
            txtDesc.Text = articles[position].Text;

            return view;
        }


    }
}