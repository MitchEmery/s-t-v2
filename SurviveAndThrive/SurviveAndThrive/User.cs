﻿using System;
using Realms;

namespace SurviveAndThrive
{
    public class User : RealmObject
    {
        //the primary key
        [PrimaryKey]
        public string UserID { get; set; } = "UserIDOne";
        //User has a user name
        public string UserName { get; set; }
        //User keeps the preferences on which campus were looking at
        public bool TrafalgarPreference { get; set; }
        public bool DavisPreference { get; set; }
        public bool HMCPreference { get; set; }

        //data to keep track of the gamification 

        public User()
        {
        }

    }
}
