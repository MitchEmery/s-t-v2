﻿using System;
using System.Linq;
using System.Collections.Generic;
using Realms;
using Newtonsoft.Json;
using System.IO;




namespace SurviveAndThrive.DataLayer
{
    public class RepositoryRealmManager : RealmManager
    {
        private bool NeedToReloadData;
        //True signifies the update check has already been run
        private static bool UpdateControl;
        public RepositoryRealmManager()
        {

            if (!UpdateControl)
            {
                //extract the payloads
                string ArticlesPayload;
                string CgPayload;

                using (StreamReader jsonReader = new StreamReader("ArticlePayload.json"))
                {
                    ArticlesPayload = jsonReader.ReadToEnd();
                }

                //extract the content gorup payloads
                using (StreamReader jsonReader = new StreamReader("ContentGroupPayload.json"))
                {
                    CgPayload = jsonReader.ReadToEnd();
                }

                NeedToReloadData = CheckForPayloadChanges(ArticlesPayload, CgPayload);

                if (base._primaryRealm.All<User>().Count() == 0 || NeedToReloadData)
                {
                    FreshInstall(ArticlesPayload, CgPayload);
                }

                UpdateControl = true;
            }

        }


        public RepositoryRealmManager(string ArticlesPayload, string CgPayload)
        {

            if (!UpdateControl)
            {
                NeedToReloadData = CheckForPayloadChanges(ArticlesPayload, CgPayload);

                if (base._primaryRealm.All<User>().Count() == 0 || CheckForPayloadChanges(ArticlesPayload, CgPayload))
                {
                    FreshInstall(ArticlesPayload, CgPayload);
                }

                UpdateControl = true;
            }
        }

       


        /// <summary>
        /// Querys the realm by the name of the content group
        /// </summary>
        /// <returns>The content group</returns>
        /// <param name="groupName">The Content Group name</param>
        public ContentGroup GetContentGroup(string groupName)
        {
            
				//query the realm for the requested Content group
				ContentGroup cg = base._primaryRealm.All<ContentGroup>().Where(x => x.Title == groupName).ElementAt(0);
				//assuming it was found, return the content group
				if (cg != null)
				{
					return cg;
				}
				else
				{
					//should never happen
					return null;
				}
        }


        /// <summary>
        /// Androids the fresh install.
        /// </summary>
        /// <param name="articlePayload">Article payload.</param>
        /// <param name="cGPayload">CGPayload.</param>
		private void FreshInstall(string articlePayload, string cgPayload)
        {


            //
            //Becuase of the realm, we break the articles and youtube videos into one file,
            //and the contentngroups into another
            //the articles that came out of the payload
            List<Article> PayloadArticles;
            //the content groups that came out of the payload
            List<ContentGroup> PayloadContentGroups;
            List<ContentGroup> ReadyContentGroups = new List<ContentGroup>();
            //extract the article payloads  
            PayloadArticles = JsonConvert.DeserializeObject<List<Article>>(articlePayload);
            //extract the content gorup payloads
            PayloadContentGroups = JsonConvert.DeserializeObject<List<ContentGroup>>(cgPayload);

            List<Article> realmArticles = base._primaryRealm.All<Article>().ToList();

            foreach (Article ra in realmArticles)
            {
                if(ra.IsFavorite || ra.IsComplete)
                {
                    foreach(Article pa in PayloadArticles)
                    {
                        if(ra.ArticleVideo.VideoID.Equals(pa.ArticleVideo.VideoID))
                        {
                            if (ra.IsFavorite)
                                pa.IsFavorite = true;
                            if (ra.IsComplete)
                                pa.IsComplete = true;
                        }
                    }
                }
            }

			base._primaryRealm.Write(() =>
			   {
				   base._primaryRealm.RemoveAll<ContentGroup>();
				   base._primaryRealm.RemoveAll<Article>();
                   base._primaryRealm.RemoveAll<YoutubeVideo>();
			   });

            if (base._primaryRealm.All<User>().Count() == 0)
            {
                //We start by adding a new user
                User u = new User();
                //defualt all prefs to true
                u.TrafalgarPreference = true;
                u.DavisPreference = true;
                u.HMCPreference = true;
                //add the user to the realm
                base._primaryRealm.Write(() => { base._primaryRealm.Add(u); });
			}


			
			foreach (ContentGroup pcg in PayloadContentGroups)
			{
				ContentGroup rcg = new ContentGroup(pcg.Title, pcg.ContentType, pcg.getSurviveFlag());
				ReadyContentGroups.Add(rcg);
			}
			

			foreach (ContentGroup cg in ReadyContentGroups)
			{

				foreach (Article a in PayloadArticles)
				{
					if (cg.ContentType == a.ContentType)
					{
						base._primaryRealm.Write(() => base._primaryRealm.Add(a.ArticleVideo));
						base._primaryRealm.Write(() => { _primaryRealm.Add(a); });
						cg.ContentArticles.Add(a);
					}
				}
				base._primaryRealm.Write(() => { _primaryRealm.Add(cg); });
			}
		}


        private bool CheckForPayloadChanges(string ArticleJSON, string CGJson)
        {

            List<Article> payloadArticles = JsonConvert.DeserializeObject<List<Article>>(ArticleJSON);
            List<Article> realmArticles = base._primaryRealm.All<Article>().ToList();

            List<ContentGroup> payloadCGs = JsonConvert.DeserializeObject<List<ContentGroup>>(CGJson);
            List<ContentGroup> realmCGs = base._primaryRealm.All<ContentGroup>().ToList();

            if ((payloadCGs.Count != realmCGs.Count) || (payloadArticles.Count != realmArticles.Count)) { return true; }


            Dictionary<Article, int> articleHash = new Dictionary<Article, int>();
            Dictionary<ContentGroup, int> cgHash = new Dictionary<ContentGroup, int>();

            foreach(Article a in payloadArticles)
            {
                if(articleHash.ContainsKey(a))
                {
                    articleHash[a]++;
                }
                else
                {
                    articleHash.Add(a, 1);
                }
            }

            foreach(Article a in realmArticles)
            {
                if(!articleHash.ContainsKey(a) || articleHash[a] == 0)
                {
                    return true;
                }
                articleHash[a]--;
            }


            foreach(ContentGroup cg in payloadCGs)
            {
                if(cgHash.ContainsKey(cg))
                {
                    cgHash[cg]++;
                }
                else
                {
                   cgHash.Add(cg, 1);
                }
            }

            foreach(ContentGroup cg in realmCGs)
            {
                if(!cgHash.ContainsKey(cg) || cgHash[cg] == 0)
                {
                    return true;   
                }
                cgHash[cg]--;
            }

            return false;

        }

    }
}
