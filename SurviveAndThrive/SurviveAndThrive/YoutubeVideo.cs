﻿using System;
using Realms;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Reflection;

using Google.Apis.Services;
using Google.Apis.YouTube.v3;
using Google.Apis.YouTube.v3.Data;

namespace SurviveAndThrive
{
    public class YoutubeVideo : RealmObject
    {
        public string VideoURL { get; set; }
        public string VideoDuration { get; set; }
        public string VideoID { get; set; }
        public string VideoEmbedURL { get; set; }

        public YoutubeVideo(string url)
        {
            getVideoID(url);
            VideoURL = url;
            getEmbededURL();

        }

        private void getVideoID(string url)
        {
            VideoID = url.Substring(32);
        }

        private void getEmbededURL()
        {
            VideoEmbedURL = "https://www.youtube.com/embed/" + VideoID + "?rel=0";
        }

        //empty cinstructor for realm
        public YoutubeVideo(){}

    }
}
