// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace SurviveAndThrive.iOS.ViewControllers
{
    [Register ("CreditsWebViewController")]
    partial class CreditsWebViewController
    {
        [Outlet]
        UIKit.UIWebView _webView { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (_webView != null) {
                _webView.Dispose ();
                _webView = null;
            }
        }
    }
}