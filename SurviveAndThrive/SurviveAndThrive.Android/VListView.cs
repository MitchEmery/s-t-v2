using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace SurviveAndThrive.Droid
{
    public class VListView : Fragment
    {
       
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            // return inflater.Inflate(Resource.Layout.YourFragment, container, false);
            View view = inflater.Inflate(Resource.Layout.VideoList, container, false);
            ListView videoListView = view.FindViewById<ListView>(Resource.Id.myListView);
            String [] items = new string[] { "Vegetables", "Fruits", "Flower Buds", "Legumes", "Bulbs", "Tubers" };
            ArrayAdapter<string> adapter = new ArrayAdapter<string>(Context, Android.Resource.Layout.SimpleListItem1, objects: items.ToArray());

            videoListView.Adapter = adapter;
            videoListView.ItemClick += detailView;
            return view;
        }

        private void detailView(object sender, AdapterView.ItemClickEventArgs e)
        {
            FragmentTransaction fragmentTxSwitch = this.FragmentManager.BeginTransaction();
            DetailFragment dfragment = new DetailFragment();
            fragmentTxSwitch.Replace(Resource.Id.fragment_container, dfragment);

            // Commit the transaction.
            fragmentTxSwitch.Commit();
        }
    }
}