using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace SurviveAndThrive.Droid
{
    public class VListViewFragment : Fragment
    {
        
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here

        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            List<Article> articles = new List<Article>();
            View view = inflater.Inflate(Resource.Layout.VideoList, container, false);
            ListView videoListView = view.FindViewById<ListView>(Resource.Id.myListView);

            for (int i = 0; i <= 3; i++)
            {
                Article a = new Article() { Title = "Welcome", Text = "Tap to watch the Welcome Video", ContentType = "welcome", IsFavorite = false };
                articles.Add(a);
            }
            //videoListView.Adapter = new ListViewAdapter(this, articles);

            videoListView.ItemClick += detailView;

            return view;
        }

        private void detailView(object sender, AdapterView.ItemClickEventArgs e)
        {
            FragmentTransaction fragmentTxSwitch = this.FragmentManager.BeginTransaction();
            DetailFragment dfragment = new DetailFragment();
            fragmentTxSwitch.Replace(Resource.Id.fragment_container, dfragment);

            // Commit the transaction.
            fragmentTxSwitch.Commit();
        }
    }
}