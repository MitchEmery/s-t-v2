// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace SurviveAndThrive.iOS
{
    [Register ("HomeViewController")]
    partial class HomeViewController
    {
        [Outlet]
        UIKit.UIButton btnFavorites { get; set; }


        [Outlet]
        UIKit.UIButton btnSearch { get; set; }


        [Outlet]
        UIKit.UIButton btnSettings { get; set; }


        [Outlet]
        UIKit.UIButton btnWelcome { get; set; }


        [Outlet]
        UIKit.UILabel lblWelcome { get; set; }


        [Outlet]
        UIKit.UIScrollView SurviveScrollView { get; set; }


        [Outlet]
        UIKit.UIScrollView ThriveScrollView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btnSurvive { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btnThrive { get; set; }


        [Action ("OnMenuItemClick:")]
        partial void OnMenuItemClick (UIKit.UIButton sender);


        [Action ("SurviveOnClick:")]
        partial void SurviveOnClick (UIKit.UIButton sender);


        [Action ("ThriveOnClick:")]
        partial void ThriveOnClick (UIKit.UIButton sender);

        void ReleaseDesignerOutlets ()
        {
            if (btnFavorites != null) {
                btnFavorites.Dispose ();
                btnFavorites = null;
            }

            if (btnSearch != null) {
                btnSearch.Dispose ();
                btnSearch = null;
            }

            if (btnSettings != null) {
                btnSettings.Dispose ();
                btnSettings = null;
            }

            if (btnSurvive != null) {
                btnSurvive.Dispose ();
                btnSurvive = null;
            }

            if (btnThrive != null) {
                btnThrive.Dispose ();
                btnThrive = null;
            }

            if (btnWelcome != null) {
                btnWelcome.Dispose ();
                btnWelcome = null;
            }

            if (lblWelcome != null) {
                lblWelcome.Dispose ();
                lblWelcome = null;
            }

            if (SurviveScrollView != null) {
                SurviveScrollView.Dispose ();
                SurviveScrollView = null;
            }

            if (ThriveScrollView != null) {
                ThriveScrollView.Dispose ();
                ThriveScrollView = null;
            }
        }
    }
}