// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace SurviveAndThrive.iOS
{
    [Register ("SearchViewController")]
    partial class SearchViewController
    {
        [Outlet]
        UIKit.UITableView _tblSearchResults { get; set; }


        [Outlet]
        UIKit.UITextField _txtFieldSearchBar { get; set; }


        [Action ("OnSearchBarChange:")]
        partial void OnSearchBarChange (Foundation.NSObject sender);

        void ReleaseDesignerOutlets ()
        {
            if (_tblSearchResults != null) {
                _tblSearchResults.Dispose ();
                _tblSearchResults = null;
            }

            if (_txtFieldSearchBar != null) {
                _txtFieldSearchBar.Dispose ();
                _txtFieldSearchBar = null;
            }
        }
    }
}