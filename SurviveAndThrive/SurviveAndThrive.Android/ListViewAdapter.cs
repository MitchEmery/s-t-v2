using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.Lang;
using Square.Picasso;
using Android.Graphics;
using Java.Net;

namespace SurviveAndThrive.Droid
{
    class ListViewAdapter : BaseAdapter<Article>
    {


        private Activity mActivity;
        public List<Article> articles { get; set; }

        int[]check;

        public ListViewAdapter(Activity mactivity, List<Article> articles)
        {
            this.mActivity = mactivity;
            this.articles = articles;
           
        }

        public override Article this[int position] => articles[position];

        public override int Count
        {
            get
            {
                return articles.Count;
            }
        }

        public override Java.Lang.Object GetItem(int position)
        {
            return null;
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {

            View view = convertView;

            if (view == null)
                view = mActivity.LayoutInflater.Inflate(Resource.Layout.VideoListRow, null);
            var title = view.FindViewById<TextView>(Resource.Id.Title);
            var time = view.FindViewById<TextView>(Resource.Id.Time);
            var campus = view.FindViewById<TextView>(Resource.Id.Campus);
            var detail = view.FindViewById<TextView>(Resource.Id.Description);
            var favs = view.FindViewById<ImageButton>(Resource.Id.Favourite);
            var thumbNail = view.FindViewById<ImageView>(Resource.Id.Thumbnail);

            // handle the difference between pressing the list cell and the favourites button
            favs.Focusable = false;
            favs.FocusableInTouchMode = false;
            favs.Clickable = true;
            favs.RefreshDrawableState();


            check = new int[articles.Count];

            if (articles[position].IsFavorite)
            {
                check[position] = 1;
                favs.SetImageResource(Resource.Drawable.ic_menu_favourite_customgold2);
            }
            else
            {
                check[position] = 0;

            }

            //thumbNail = articles[position].ArticleVideo.;


            title.Text = articles[position].Title;
            detail.Text = "Tap to watch a " + articles[position].ArticleVideo.VideoDuration + " video or read the text.";
            time.Text = articles[position].ArticleVideo.VideoDuration;
            if(articles[position]._campusFilter == 3)
                campus.Text = "Trafalgar";
            else if(articles[position]._campusFilter == 2)
                campus.Text = "Davis";
            else if (articles[position]._campusFilter == 1)
                campus.Text = "HMC";
            else
                campus.Text = "All";

            if (view == null)
            {
                view = new ImageView(Application.Context);
            }

            string url = "https://img.youtube.com/vi/" + articles[position].ArticleVideo.VideoID + "/mqdefault.jpg";
            Picasso.With(Application.Context)
                .Load(url)
                .Placeholder(Resource.Drawable.AA)
                .Error(Resource.Drawable.AA)
                .Resize(200, 200)
                .CenterCrop()
                .Into(thumbNail);

            // add article to favourite
            //favs.Click += delegate
            //{

            //    if (favs.Pressed && check[position] == 0)
            //    {
            //        articles[position].UpdateFavorite(true);
            //        favs.SetImageResource(Resource.Drawable.ic_menu_favourite_customgold2);
            //        check[position] = 1;
            //        //string a = articles[position].IsFavorite.ToString();
            //        //Toast.MakeText(this, a, ToastLength.Short).Show();
            //        //Activity activityObj = this.mActivity;
            //        //Toast.MakeText(activityObj, a, ToastLength.Short).Show();

            //    }
            //    else if (favs.Pressed && check[position] == 1)
            //    {
            //        articles[position].UpdateFavorite(false);
            //        favs.SetImageResource(Resource.Drawable.ic_menu_favourite_customgold1);
            //        check[position] = 0;
            //        //string a = articles[position].IsFavorite.ToString();
            //        //Activity activityObj = this.mActivity;
            //        //Toast.MakeText(activityObj, a, ToastLength.Short).Show();
            //    }


            //};
            //buttonpressed--;

            if (convertView == null)
            {
                favs.Click += (o, e) =>
                {

                    if (favs.Pressed && check[position] == 0)
                    {

                        articles[position].UpdateFavorite(true);
                        favs.SetImageResource(Resource.Drawable.ic_menu_favourite_customgold2);
                        check[position] = 1;

                        //Toast.MakeText(this, a, ToastLength.Short).Show();
                        //Activity activityObj = this.mActivity;
                        //Toast.MakeText(activityObj, a, ToastLength.Short).Show();

                    }
                    else if (favs.Pressed && check[position] == 1)
                    {
                        articles[position].UpdateFavorite(false);
                        favs.SetImageResource(Resource.Drawable.ic_menu_favourite_customgold1);
                        check[position] = 0;
                        //string a = articles[position].IsFavorite.ToString();
                        //Activity activityObj = this.mActivity;
                        //Toast.MakeText(activityObj, a, ToastLength.Short).Show();

                    }
                };
            }
            return view;
        }

        public void UpdateArticles(List<Article> article)
        {
            this.articles = article;
            base.NotifyDataSetChanged();
        }


    }

}