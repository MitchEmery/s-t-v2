// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace SurviveAndThrive.iOS
{
    [Register ("SettingsViewController")]
    partial class SettingsViewController
    {
        [Outlet]
        public UIKit.UIProgressView _completionTracker { get; set; }

        [Outlet]
        public UIKit.UISwitch DavisToggle { get; set; }

        [Outlet]
        public UIKit.UISwitch HMCToggle { get; set; }

        [Outlet]
        public UIKit.UISwitch TrafalgarToggle { get; set; }

        [Action ("DavisToggleValueChange:")]
        partial void DavisToggleValueChange (UIKit.UISwitch sender);

        [Action ("HMCToggleValueChanged:")]
        partial void HMCToggleValueChanged (UIKit.UISwitch sender);

        [Action ("OnCreditsClick:")]
        partial void OnCreditsClick (Foundation.NSObject sender);

        [Action ("SwipeBack:")]
        partial void SwipeBack (UIKit.UISwipeGestureRecognizer sender);

        [Action ("TrafToggleValueChange:")]
        partial void TrafToggleValueChange (UIKit.UISwitch sender);

        [Action ("_txtUserName:")]
        partial void UpdateUserName (Foundation.NSObject sender);
        
        void ReleaseDesignerOutlets ()
        {
            if (DavisToggle != null) {
                DavisToggle.Dispose ();
                DavisToggle = null;
            }

            if (HMCToggle != null) {
                HMCToggle.Dispose ();
                HMCToggle = null;
            }

            if (TrafalgarToggle != null) {
                TrafalgarToggle.Dispose ();
                TrafalgarToggle = null;
            }

            if (_completionTracker != null) {
                _completionTracker.Dispose ();
                _completionTracker = null;
            }
        }
    }
}
