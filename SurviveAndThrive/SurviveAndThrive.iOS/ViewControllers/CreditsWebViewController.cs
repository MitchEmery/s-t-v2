﻿using System;
using System.CodeDom.Compiler;
using UIKit;
using Foundation;
using System.IO;

namespace SurviveAndThrive.iOS.ViewControllers
{
    public partial class CreditsWebViewController : UIViewController
    {
        public CreditsWebViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.
            //var htmlFile = NSBundle.MainBundle.PathForResource("Credits", "html");
			string fileName = "Credits.html"; // remember case-sensitive
			string localHtmlUrl = Path.Combine(NSBundle.MainBundle.BundlePath, fileName);
            _webView.LoadRequest(new NSUrlRequest(new NSUrl(localHtmlUrl, false)));
			//_webView.ScalesPageToFit = false;
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }
    }
}

