using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using SurviveAndThrive.DataLayer;
using Newtonsoft.Json;

namespace SurviveAndThrive.Droid
{
    public class SurviveFragment : Fragment
    {
        // the content repo will hold the repository
       // List<ImageButton> contentList = new List<ImageButton>();
        List<TextView> contentTitle = new List<TextView>();
        //VListView listfragment = new VListView();

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
           
            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            // return inflater.Inflate(Resource.Layout.YourFragment, container, false);
            View view = inflater.Inflate(Resource.Layout.Survive, container, false);
            
            ImageButton welcomebtn = view.FindViewById<ImageButton>(Resource.Id.imageButton1);
            ImageButton financebtn = view.FindViewById<ImageButton>(Resource.Id.imageButton2);
            ImageButton regbtn = view.FindViewById<ImageButton>(Resource.Id.imageButton3);
            ImageButton housingbtn = view.FindViewById<ImageButton>(Resource.Id.imageButton4);
            ImageButton transbtn = view.FindViewById<ImageButton>(Resource.Id.imageButton5);
            ImageButton healthbtn = view.FindViewById<ImageButton>(Resource.Id.imageButton6);
            ImageButton stuserbtn = view.FindViewById<ImageButton>(Resource.Id.imageButton7);
            ImageButton foodbtn = view.FindViewById<ImageButton>(Resource.Id.imageButton8);
            ImageButton onecardbtn = view.FindViewById<ImageButton>(Resource.Id.imageButton9);
            ImageButton itsupportbtn = view.FindViewById<ImageButton>(Resource.Id.imageButton10);

            contentTitle.Add(view.FindViewById<TextView>(Resource.Id.textView1));
            contentTitle.Add(view.FindViewById<TextView>(Resource.Id.textView2));
            contentTitle.Add(view.FindViewById<TextView>(Resource.Id.textView3));
            contentTitle.Add(view.FindViewById<TextView>(Resource.Id.textView4));
            contentTitle.Add(view.FindViewById<TextView>(Resource.Id.textView5));
            contentTitle.Add(view.FindViewById<TextView>(Resource.Id.textView6));
            contentTitle.Add(view.FindViewById<TextView>(Resource.Id.textView7));
            contentTitle.Add(view.FindViewById<TextView>(Resource.Id.textView8));
            contentTitle.Add(view.FindViewById<TextView>(Resource.Id.textView9));
            contentTitle.Add(view.FindViewById<TextView>(Resource.Id.textView10));

            //for (int i = contentTitle.Count(); i < 10; i++)
            //{
            //    processCLick(i);
            //    //contentList[i].Click += delegate
            //    //{
            //    //    Bundle b = new Bundle();

            //    //    //Activity.ActionBar.Title = "Welcome";

            //    //    Intent intent = new Intent(this.Activity, typeof(ListActivity));
            //    //    ContentGroup CG = _repoRealmManager.GetContentGroup(contentTitle[i].Text);
            //    //    //b.PutParcelable("CG", CG);
            //    //    intent.PutExtra("title", contentTitle[i].Text);
            //    //    intent.PutExtra("content", b);
            //    //    //intent.PutExtra("content", CG);       
            //    //    StartActivity(intent);


            //    //};
            //}

            welcomebtn.Click += delegate
            {
                Intent intent = new Intent(this.Activity, typeof(ListActivity));           
                intent.PutExtra("title", contentTitle[0].Text);
                StartActivity(intent);
            };

            financebtn.Click += delegate
            {
                Bundle b = new Bundle();
                Intent intent = new Intent(this.Activity, typeof(ListActivity));
                intent.PutExtra("title", contentTitle[1].Text);
                StartActivity(intent);
            };

            regbtn.Click += delegate
            {
                Bundle b = new Bundle();
                Intent intent = new Intent(this.Activity, typeof(ListActivity));
                intent.PutExtra("title", contentTitle[2].Text);      
                StartActivity(intent);
            };

            housingbtn.Click += delegate
            {
                Bundle b = new Bundle();
                Intent intent = new Intent(this.Activity, typeof(ListActivity));
                intent.PutExtra("title", contentTitle[3].Text);
                StartActivity(intent);
            };

            transbtn.Click += delegate
            {
                Bundle b = new Bundle();
                Intent intent = new Intent(this.Activity, typeof(ListActivity));
                intent.PutExtra("title", contentTitle[4].Text);     
                StartActivity(intent);
            };
      
            healthbtn.Click += delegate
            {
                Bundle b = new Bundle();
                Intent intent = new Intent(this.Activity, typeof(ListActivity));
                intent.PutExtra("title", contentTitle[5].Text);    
                StartActivity(intent);
            };

            stuserbtn.Click += delegate
            {
                Bundle b = new Bundle();
                Intent intent = new Intent(this.Activity, typeof(ListActivity));
                intent.PutExtra("title", contentTitle[6].Text);   
                StartActivity(intent);
            };

            foodbtn.Click += delegate
            {
                Bundle b = new Bundle();

                Intent intent = new Intent(this.Activity, typeof(ListActivity));
                //ContentGroup CG = _repoRealmManager.GetContentGroup(contentTitle[1].Text);
                //b.PutParcelable("CG", CG);
                intent.PutExtra("title", contentTitle[7].Text);
                //intent.PutExtra("content", b);
                //intent.PutExtra("content", CG);       
                StartActivity(intent);
            };

            onecardbtn.Click += delegate
            {
                Bundle b = new Bundle();

                Intent intent = new Intent(this.Activity, typeof(ListActivity));
                //ContentGroup CG = _repoRealmManager.GetContentGroup(contentTitle[1].Text);
                //b.PutParcelable("CG", CG);
                intent.PutExtra("title", contentTitle[8].Text);
                //intent.PutExtra("content", b);
                //intent.PutExtra("content", CG);       
                StartActivity(intent);
            };

            itsupportbtn.Click += delegate
            {
                Bundle b = new Bundle();

                Intent intent = new Intent(this.Activity, typeof(ListActivity));
                //ContentGroup CG = _repoRealmManager.GetContentGroup(contentTitle[1].Text);
                //b.PutParcelable("CG", CG);
                intent.PutExtra("title", contentTitle[9].Text);
                //intent.PutExtra("content", b);
                //intent.PutExtra("content", CG);       
                StartActivity(intent);
            };

            return view;
            
        }
        //public void processCLick(int i)
        //{
        //    contentList[i].Click += delegate
        //    {
        //        Bundle b = new Bundle();

        //        //Activity.ActionBar.Title = "Welcome";

        //        Intent intent = new Intent(this.Activity, typeof(ListActivity));
        //        ContentGroup CG = _repoRealmManager.GetContentGroup(contentTitle[i].Text);
        //        //String jsonStrong = ToJSON(CG);
        //        //b.PutParcelable("CG", CG);
        //        intent.PutExtra("title", contentTitle[i].Text);
        //        //intent.PutExtra("content", jsonStrong);
        //        //intent.PutExtra("content", CG);       
        //        StartActivity(intent);


        //    };
        //}

   
    }
}