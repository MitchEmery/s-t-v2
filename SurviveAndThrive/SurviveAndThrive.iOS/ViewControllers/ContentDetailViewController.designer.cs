// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace SurviveAndThrive.iOS
{
    [Register ("ContentDetailViewController")]
    partial class ContentDetailViewController
    {
        [Outlet]
        UIKit.UITableView _articleTable { get; set; }


        [Outlet]
        UIKit.UIButton _btnFavorites { get; set; }


        [Outlet]
        public UIKit.UILabel lblContentTitle { get; private set; }


        [Action ("OnBackButtonPress:")]
        partial void OnBackButtonPress (Foundation.NSObject sender);


        [Action ("OnFavoritesClick:")]
        partial void OnFavoritesClick (Foundation.NSObject sender);

        void ReleaseDesignerOutlets ()
        {
            if (_articleTable != null) {
                _articleTable.Dispose ();
                _articleTable = null;
            }

            if (_btnFavorites != null) {
                _btnFavorites.Dispose ();
                _btnFavorites = null;
            }

            if (lblContentTitle != null) {
                lblContentTitle.Dispose ();
                lblContentTitle = null;
            }
        }
    }
}