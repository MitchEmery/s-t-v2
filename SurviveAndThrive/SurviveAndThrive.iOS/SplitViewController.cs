﻿using System;

using UIKit;

namespace SurviveAndThrive.iOS
{
    public partial class SplitViewController : UISplitViewController
    {
        UIViewController master, detail;

        public SplitViewController() : base()
        {
            master = new MasterViewController();
            detail = new DetailViewController();


            ViewControllers = new UIViewController[] { detail, master };
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }
    }
}

