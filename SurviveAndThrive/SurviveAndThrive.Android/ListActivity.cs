﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Com.Balysv.Material.Drawable.Menu;
using Android.Graphics;
using static Android.Views.View;
using Android.Util;
using SurviveAndThrive.DataLayer;
using Newtonsoft.Json;
using Android.Content.Res;
using System.IO;
using Android.Webkit;
using static Android.Webkit.WebSettings;
using Firebase.Analytics;

namespace SurviveAndThrive.Droid
{
    
    [Activity(Label = "ListActivity")]
    public class ListActivity : Activity, IOnClickListener
    {
        string title;
        Toolbar toolbar;
        ListView videoListView;
        List<Article> articles;
        int actionBarMenuState;
        Bundle bundle;
        Article currentArticle;
        List<Article> favouriteArticles;
        RealmManager realm;
        private RepositoryRealmManager _repoRealmManager;
        ContentGroup CG;
        //ListViewAdapter videoAdapter;
        FirebaseAnalytics fba;
        enum Stroke
        {
            REGULAR = 3,
            THIN = 2,
            EXTRA_THIN = 1
        }
        public static readonly string Tag = typeof(ListActivity).ToString();

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            fba = FirebaseAnalytics.GetInstance(this);
            // Create your application here
            SetContentView(Resource.Layout.VideoList);
            videoListView = FindViewById<ListView>(Resource.Id.myListView);
            articles = new List<Article>();
            //isSearched = bundle.GetBoolean("isSearched");
            toolbar = FindViewById<Toolbar>(Resource.Id.toolbar2);
            SetActionBar(toolbar);
            string payload;
            string payload2;
            AssetManager assets = Android.App.Application.Context.Assets;
            using (StreamReader jsonReader = new StreamReader(assets.Open("ArticlePayload.json")))
            {
                payload = jsonReader.ReadToEnd();
            }

            using (StreamReader jsonReader = new StreamReader(assets.Open("ContentGroupPayload.json")))
            {
                payload2 = jsonReader.ReadToEnd();
            }
            _repoRealmManager = new RepositoryRealmManager(payload, payload2);

            title = Intent.GetStringExtra("title");
            string content = Intent.GetStringExtra("content");

            ActionBar.Title = title;
            // populate the listview with data
            // // to be defined



            //for (int i = 0; i <= 3; i++)
            //{
            //    Article a = new Article() { Title = "Welcome", Text = "Tap to watch the Welcome Video", ContentType = "welcome", IsFavorite = false };
            //    articles.Add(a);
            //}

            // getting content group based on which button group is pressed and the passed title
            if(title.Equals("Favourite"))
            {
                realm = new RealmManager();
            }
            else
            {
                CG = _repoRealmManager.GetContentGroup(title);
            }
            List<Campus> c = new List<Campus>();

            // adding articles with the all tag
            c.Add(Campus.All);

            // depending and user preference adding the articles based on which campus they prefer
            if (_repoRealmManager.GetUser().TrafalgarPreference)
            {
                c.Add(Campus.Trafalgar);

            }
            if (_repoRealmManager.GetUser().HMCPreference)
            {
                c.Add(Campus.HMC);

            }

            if (_repoRealmManager.GetUser().DavisPreference)
            {
                c.Add(Campus.Davis);

            }
            // if favourite then getting the favourtie list else 
            // setting preferences
            // when clicked we go to the detail view page method
            if (title.Equals("Favourite"))
            {
                favouriteArticles = realm.RetrieveFavourites();
                videoListView.Adapter = new ListViewAdapter(this, favouriteArticles);
                videoListView.ItemClick += detailViewFavourite;
            }
            else
            {
                CG.SetFilterList(c);
                videoListView.Adapter = new ListViewAdapter(this, CG.getFilteredList());
                videoListView.ItemClick += detailView;
<<<<<<< HEAD

            }
            
=======
            }

>>>>>>> 71b882a17e68ceb957980780a6bf72f31847499d
            //MaterialMenuDrawable materialMenu = new MaterialMenuDrawable(this, Color.White, (int)Stroke.EXTRA_THIN,
            //    MaterialMenuDrawable.DefaultScale,
            //    MaterialMenuDrawable.DefaultTransformDuration);
            //toolbar.NavigationIcon = materialMenu;

            //toolbar.SetNavigationOnClickListener(this);
            //{
            //};

        }

        private object getIntent()
        {
            throw new NotImplementedException();
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            if(title.Equals("Favourite"))
                MenuInflater.Inflate(Resource.Menu.menu_searchonly, menu);
            else 
                MenuInflater.Inflate(Resource.Menu.top_menus, menu);
            return base.OnCreateOptionsMenu(menu);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            //Toast.MakeText(this, "Action selected: " + item.TitleFormatted,ToastLength.Short).Show();
            TextView info = FindViewById<TextView>(Resource.Id.Info);
            WebView links = FindViewById<WebView>(Resource.Id.Links);
            FrameLayout detailContainer = (FrameLayout)FindViewById(Resource.Id.frameLayout1);
            WebSettings settings = links.Settings;
            
            settings.GetTextSize();
            // handling size of text when the details fragment is not up yet.
            float sizeOfText = 20;
            
            try
            {
                //sizeOfText = info.TextSize;
                //sizeOfText = (float)settings.GetTextSize();
                sizeOfText = (float)settings.TextZoom;
                //settings.DefaultFontSize =15;
            }
            catch (Exception e)
            {
                Console.WriteLine("error" + e);
                sizeOfText = 20;
            }



            // this will handle the content text size also added a handle to make sure the the font does not go too small
            switch (item.ItemId)
            {
                
                case Resource.Id.plus_font:
                    //sizeOfText = (float)settings.GetTextSize();
                    float increaseSize = sizeOfText + 10;
                    //settings.DefaultFontSize = (int)increaseSize;
                    settings.TextZoom = (int)increaseSize;
                    //info.SetTextSize(Android.Util.ComplexUnitType.Px, increaseSize);
                    //links.Settings.SetTextSize(SMALLEST);
                    break;
                case Resource.Id.minus_font:
                    //sizeOfText = (float)settings.GetTextSize();
                    float decreaseSize = sizeOfText - 10;
                    //settings.DefaultFontSize = (int)decreaseSize;
                    
                    if (decreaseSize > 40)
                        //info.SetTextSize(Android.Util.ComplexUnitType.Px, decreaseSize);
                        settings.TextZoom = (int)decreaseSize;
                    break;
                case Resource.Id.menu_search:
                    //FragmentTransaction fragmentTxSwitch = this.FragmentManager.BeginTransaction();
                    //BlankFragment b = new BlankFragment();
                    ////fragmentTxSwitch.Hide();
                    //fragmentTxSwitch.Replace(Resource.Id.frameLayout1, b);
                    //fragmentTxSwitch.SetTransition(FragmentTransit.FragmentFade);
                    //// Commit the transaction.
                    //fragmentTxSwitch.Commit();
                    Intent intent = new Intent(this, typeof(SearchActivity));
                    //intent.PutExtra("title", "Search");
                    StartActivity(intent);
                    break;
                case Resource.Id.menu_favourite:
                    Intent intent2 = new Intent(this, typeof(ListActivity));
                    intent2.PutExtra("title", "Favourite");
                    StartActivity(intent2);
                    break;
                default://invalid option
                    Toast.MakeText(this, "Error", ToastLength.Short).Show();
                    break;
            }

            //ActionBar.Title = "CATAGORY TBD";


            return base.OnOptionsItemSelected(item);
        }


        public void OnClick(View v)
        {
            //GetMaterialMenu(toolbar).AnimateIconState(IntToState(actionBarMenuState), false);
        }
        //private MaterialMenuDrawable GetMaterialMenu(Toolbar toolbar)
        //{
        //    return (MaterialMenuDrawable)toolbar.NavigationIcon;
        //}
        //private MaterialMenuDrawable.IconState IntToState(int currentactionBarMenuState)
        //{
        //    if (currentactionBarMenuState == 0)
        //    {
        //        // show details page
        //        actionBarMenuState = 1;
        //        FragmentTransaction fragmentTxCampus = this.FragmentManager.BeginTransaction();
        //        DetailFragment dfragment = new DetailFragment();
        //        fragmentTxCampus.Replace(Resource.Id.frameLayout1, dfragment);
        //        fragmentTxCampus.SetTransition(FragmentTransit.FragmentFade);
        //        // Commit the transaction.
        //        fragmentTxCampus.Commit();
        //        return MaterialMenuDrawable.IconState.Arrow;

        //    }
        //    else if (currentactionBarMenuState == 1)
        //    {
        //        // hide details page
        //        //actionBarMenuState = 0;
        //        FragmentTransaction fragmentTxCampus = this.FragmentManager.BeginTransaction();
        //        BlankFragment blank = new BlankFragment();
        //        CampusFragment cfragment = new CampusFragment();
        //        fragmentTxCampus.Replace(Resource.Id.frameLayout1, blank);
        //        fragmentTxCampus.SetTransition(FragmentTransit.FragmentFade);
        //        // Commit the transaction.
        //        fragmentTxCampus.Commit();
        //        return MaterialMenuDrawable.IconState.Check;
        //    }
        //    else
        //    {
        //        return MaterialMenuDrawable.IconState.Check;
        //    }
        //}

        // going to detail fragment and transfering the article as well
        private void detailView(object sender, AdapterView.ItemClickEventArgs e)
        {
            long p = videoListView.GetItemIdAtPosition(e.Position);

            currentArticle = CG.ContentArticles[(int)p];
            
            
            string save = ToJSON(currentArticle);
            bundle = new Bundle();
            bundle.PutString("article", save);

            //videoListView.Visibility = ViewStates.Invisible;
            FragmentTransaction fragmentTxSwitch = this.FragmentManager.BeginTransaction();
            DetailFragment dfragment = new DetailFragment();
            dfragment.Arguments = bundle;


            fragmentTxSwitch.AddToBackStack(null);
            fragmentTxSwitch.Replace(Resource.Id.frameLayout1, dfragment);
            AssetManager assets = Android.App.Application.Context.Assets;
<<<<<<< HEAD
            Bundle bundleAnalytics = new Bundle();
            bundleAnalytics.PutString(FirebaseAnalytics.Param.ItemId, currentArticle.ContentType);
            bundleAnalytics.PutString(FirebaseAnalytics.Param.ItemName, currentArticle.Title);

            fba.LogEvent(FirebaseAnalytics.Event.SelectContent, bundleAnalytics);
=======

>>>>>>> 71b882a17e68ceb957980780a6bf72f31847499d
            // Commit the transaction.
            fragmentTxSwitch.Commit();

        }

        private void detailViewFavourite(object sender, AdapterView.ItemClickEventArgs e)
        {
            long p = videoListView.GetItemIdAtPosition(e.Position);
                currentArticle = favouriteArticles[(int)p];


            string save = ToJSON(currentArticle);
            Bundle bundle = new Bundle();
            bundle.PutString("article", save);

            //videoListView.Visibility = ViewStates.Invisible;
            FragmentTransaction fragmentTxSwitch = this.FragmentManager.BeginTransaction();
            DetailFragment dfragment = new DetailFragment();
            dfragment.Arguments = bundle;


            fragmentTxSwitch.AddToBackStack(null);
            fragmentTxSwitch.Replace(Resource.Id.frameLayout1, dfragment);
            AssetManager assets = Android.App.Application.Context.Assets;
<<<<<<< HEAD
            Bundle bundleAnalytics = new Bundle();
            bundleAnalytics.PutString(FirebaseAnalytics.Param.ItemId, currentArticle.ContentType);
            bundleAnalytics.PutString(FirebaseAnalytics.Param.ItemName, currentArticle.Title);

            fba.LogEvent(FirebaseAnalytics.Event.SelectContent, bundleAnalytics);
=======

>>>>>>> 71b882a17e68ceb957980780a6bf72f31847499d
            // Commit the transaction.
            fragmentTxSwitch.Commit();

        }


        //static public ContentGroup FromJSON<ContentGroup>(string code)
        //{
        //    var item = JsonConvert.DeserializeObject<ContentGroup>(code);
        //    return item;
        //}

        static public string ToJSON(Article item)
        {
            var myval = JsonConvert.SerializeObject(item);
            return myval;
        }

    }


}