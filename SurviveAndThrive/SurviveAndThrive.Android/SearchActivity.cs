﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Content.Res;
using Newtonsoft.Json;
using Android.Views.InputMethods;
using Android.Webkit;

namespace SurviveAndThrive.Droid
{
    [Activity(Label = "SearchActivity")]
    public class SearchActivity : Activity
    {
        private SearchView _search;
        private ListView _listView;
        private Article currentArticle;
        private List<Article> searchedArticles;
        private RealmManager _realm;
        private Toolbar toolbar;
        ListViewAdapter adapter;
        Bundle bundle;
        private bool isSearch = false;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);         
            SetContentView(Resource.Layout.Search);
            // Create your application here
            toolbar = FindViewById<Toolbar>(Resource.Id.toolbarSearch);
            SetActionBar(toolbar);
            ActionBar.Title = "";
            _search = FindViewById<SearchView>(Resource.Id.searchView1);
            _search.SetIconifiedByDefault(false);
            _search.RequestFocusFromTouch();
            _search.RequestFocus();
            _realm = new RealmManager();
            searchedArticles = new List<Article>();


            _listView = FindViewById<ListView>(Resource.Id.searchListView);
            checkResult();
            _listView.ItemClick += detailViewSearch;
        }

        private void detailViewSearch(object sender, AdapterView.ItemClickEventArgs e)
        {
            isSearch = true;
            string a = e.ToString();
            Console.WriteLine("test " +a);
            InputMethodManager inputManager = (InputMethodManager)this.GetSystemService(Context.InputMethodService);

            inputManager.HideSoftInputFromWindow(this.CurrentFocus.WindowToken, HideSoftInputFlags.NotAlways);
            long p = _listView.GetItemIdAtPosition(e.Position);
            currentArticle = searchedArticles[(int)p];
            string save = ToJSON(currentArticle);
            bundle = new Bundle();
            bundle.PutString("article", save);
            bundle.PutBoolean("isSearched", isSearch);
            //videoListView.Visibility = ViewStates.Invisible;
            FragmentTransaction fragmentTxSwitch = this.FragmentManager.BeginTransaction();
            
            DetailFragment dfragment = new DetailFragment();
            dfragment.Arguments = bundle;
            
            fragmentTxSwitch.AddToBackStack(null);
            fragmentTxSwitch.Replace(Resource.Id.frameLayout1, dfragment);
            AssetManager assets = Android.App.Application.Context.Assets;

            // Commit the transaction.
            fragmentTxSwitch.Commit();
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            TextView info = FindViewById<TextView>(Resource.Id.Info);

            // handling size of text when the details fragment is not up yet.
            float sizeOfText = 0;
            try
            {
                sizeOfText = info.TextSize;
            }
            catch (Exception e)
            {
                //Console.WriteLine("error" + e);
                sizeOfText = 20;
            }

            switch (item.ItemId)
            {
                case Resource.Id.plus_font:
                    float increaseSize = sizeOfText + 10;
                    info.SetTextSize(Android.Util.ComplexUnitType.Px, increaseSize);
                    break;
                case Resource.Id.minus_font:
                    float decreaseSize = sizeOfText - 10;
                    if (decreaseSize > 15)
                        info.SetTextSize(Android.Util.ComplexUnitType.Px, decreaseSize);
                    break;
                default://invalid option
                    Toast.MakeText(this, "Error", ToastLength.Short).Show();
                    break;
            }
            return base.OnOptionsItemSelected(item);
        }

        public class MyWebViewClient : WebViewClient
        {
            Boolean loadingFinished = true;
            Boolean redirect = false;
            public override bool ShouldOverrideUrlLoading(WebView views, IWebResourceRequest request)
            {
                views.LoadUrl(request.Url.ToString());
                return false;
            }
        }


            //public override bool OnCreateOptionsMenu(IMenu menu)
            //{


            //    if (isSearch)
            //        MenuInflater.Inflate(Resource.Menu.size_only, menu);
            //    else
            //        MenuInflater.Inflate(Resource.Menu.clear_menu, menu);
            //return base.OnCreateOptionsMenu(menu);
            //}


            //private void detailViewSearch(object sender, AdapterView.ItemClickEventArgs e)
            //{
            //    bool isSearch = false;
            //    InputMethodManager inputManager = (InputMethodManager)this.GetSystemService(Context.InputMethodService);
            //    inputManager.HideSoftInputFromWindow(this.CurrentFocus.WindowToken, HideSoftInputFlags.NotAlways);
            //    long p = _listView.GetItemIdAtPosition(e.Position);
            //    currentArticle = searchedArticles[(int)p];

            //    string save = ToJSON(currentArticle);
            //    isSearch = true;
            //    Intent intent = new Intent(this, typeof(ListActivity));
            //    intent.PutExtra("article", save);
            //    intent.PutExtra("searched", isSearch);
            //    intent.PutExtra("title", currentArticle.Title);
            //    StartActivity(intent);
            //}

            private void checkResult()
        {
            // handles the search on submit and on text change
            _search.QueryTextSubmit += (sender, e) =>
                {
                    string result = e.Query;
                    if (result != null && result.Trim() != "")
                    {
                        searchedArticles = _realm.SearchArtices(result);
                        adapter = new ListViewAdapter(this, searchedArticles);
                        adapter.UpdateArticles(searchedArticles);
                        _listView.Adapter = adapter;
                        //InputMethodManager imm = (InputMethodManager)GetSystemService(Context.InputMethodService);
                        //imm.HideSoftInputFromWindow(_search.WindowToken, 0);



                    }
                    e.Handled = true;
                };
            _search.QueryTextChange += (sender, e) =>
            {
                string result = e.NewText;
                if (result != null && result.Trim() != "")
                {
                    searchedArticles = _realm.SearchArtices(result);

                    adapter = new ListViewAdapter(this, searchedArticles);
                    adapter.UpdateArticles(searchedArticles);
                    _listView.Adapter = adapter;  
                }
                e.Handled = true;
            };


        }

        static public string ToJSON(Article item)
        {
            var myval = JsonConvert.SerializeObject(item);
            return myval;
        }

    }
}