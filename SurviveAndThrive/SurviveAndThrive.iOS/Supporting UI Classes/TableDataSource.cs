﻿using System;
using Realms;
using System.Collections.Generic;
using Foundation;
using UIKit;
namespace SurviveAndThrive.iOS
{
    public class TableDataSource : UITableViewSource
    {
        private List<Article> _articleDataList;
        const string _reuseID = "ReuseIdentifier";

        public TableDataSource(List<Article> artList)
        {
            _articleDataList = artList;
        }

        public void updateSearchList(List<Article> newList)
        {
            _articleDataList = newList;
        }

        public List<Article> getCurrentDisplayedList()
        {
            return _articleDataList;
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            ContentDetailCell cell = (ContentDetailCell)tableView.DequeueReusableCell(_reuseID);

            Article currArt = _articleDataList[indexPath.Row];

            //if there are no cells to reuse create a new one
            if(cell == null)
            {
                cell = new ContentDetailCell();
            }

            if (currArt.IsFavorite)
            {
                cell._btnFavorites.SetImage(new UIImage("FavoriteIconSelected.png"), UIControlState.Normal);
            }

            if (cell._btnFavorites.AllTargets.Count == 0)
            {
                //add a event handler for the favorites button
                cell._btnFavorites.TouchUpInside += (sender, e) =>
                {

                    if (!currArt.IsFavorite)
                    {
                        currArt.UpdateFavorite(true);
                        cell._btnFavorites.SetImage(new UIImage("FavoriteIconSelected.png"), UIControlState.Normal);
                    }
                    else
                    {
                        currArt.UpdateFavorite(false);
                        cell._btnFavorites.SetImage(new UIImage("FavIcon.png"), UIControlState.Normal);
                    }


                };
            }

            //currArt.ArticleVideo.RetrieveVideoDetails();

            cell._lblTitle.Text = currArt.Title;
            cell._lblTime.Text = currArt.ArticleVideo.VideoDuration;

            if (currArt.ArticleVideo.VideoDuration != null)
            {
				cell._lblArticleDesc.Text = "Tap to watch a " + currArt.ArticleVideo.VideoDuration + " minute " + currArt.Title + " video or read the text.";
			}
            else
            {
				cell._lblArticleDesc.Text = "Tap to watch a video or read the text.";
			}

            switch(currArt.getCampusFilter())
            {
                case Campus.All:
                    cell._lblcampus.Text = "";
                    break;
                
                case Campus.HMC:
                    cell._lblcampus.Text = "HMC";
                    break;

                case Campus.Davis:
                    cell._lblcampus.Text = "Davis";
                    break;


                case Campus.Trafalgar:
                    cell._lblcampus.Text = "Trafalgar";
                    break;
            }



            NSData imageData = Foundation.NSData.FromUrl(new NSUrl("https://img.youtube.com/vi/" + currArt.ArticleVideo.VideoID + "/mqdefault.jpg"));

            if(imageData != null)
            {
                cell._imgThumbnail.Image = new UIImage(imageData);
            }
            else
            {
                cell._imgThumbnail.Image = new UIImage("NoWifi.png");   
            }

            return cell;

        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return _articleDataList.Count;
        }

        public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
        {
            return 100;
        }

       
    }
}
