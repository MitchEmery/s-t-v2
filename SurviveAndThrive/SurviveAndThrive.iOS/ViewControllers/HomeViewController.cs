﻿using System;
using System.Collections.Generic;
using Foundation;
using UIKit;
using System.CodeDom.Compiler;

using Firebase.Analytics;
using SurviveAndThrive.DataLayer;

namespace SurviveAndThrive.iOS
{
    public partial class HomeViewController : UIViewController
    {
        //The constant for the Segue id's
        const string SettingSegueID = "SettingsModalSegue";
        //filter bool variable
        public bool TrafFilter { get; set; }
        public bool HMCFilter { get; set; }
        public bool DavisFilter { get; set; }

        //The content repo property
        private RepositoryRealmManager _repoRealmManager;

        public HomeViewController(IntPtr handle) : base(handle)
        {
            //intialize the realm manager
            _repoRealmManager = new RepositoryRealmManager();

            //grab the user defaults from the user object in the realm
            TrafFilter = _repoRealmManager.GetUser().TrafalgarPreference;
            HMCFilter = _repoRealmManager.GetUser().HMCPreference;
            DavisFilter = _repoRealmManager.GetUser().DavisPreference;
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.

            //Additonal UI Setup
            //round Button Corners
            btnThrive.Layer.CornerRadius = 10;
            btnSurvive.Layer.CornerRadius = 10;
            //The survive scrollview is shown first
            btnSurvive.Layer.BorderWidth = 3;
            btnSurvive.Layer.BorderColor = UIColor.White.CGColor;
            //Add the gray border to the views
            foreach (UIView v in SurviveScrollView.Subviews[0])
            {
                foreach (UIView vi in v.Subviews)
                {
                    vi.Layer.BorderColor = UIColor.Gray.CGColor;
                    vi.Layer.BorderWidth = 1;
                }
            }
            //Add the gray border to the views
            foreach (UIView v in ThriveScrollView.Subviews[0])
            {
                foreach (UIView vi in v.Subviews)
                {
                    
                    vi.Layer.BorderColor = UIColor.Gray.CGColor;
                    vi.Layer.BorderWidth = 1;
                    
                }
            }
		}

		/// <summary>
		/// Prepares for segue.
		/// </summary>
		/// <param name="segue">Segue.</param>
		/// <param name="sender">Sender.</param>
        public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
        {
            //check to see which view controller is the destination
            base.PrepareForSegue(segue, sender);

            //check to see if the destination is the content detail page
            if(segue.Identifier == "ContentDetailSegue")
            {
                //grab the destination View Controller
                var destinationContentVC = (ContentDetailViewController)segue.DestinationViewController;

                var senderView = (UIView)sender;

                var labelText = (UILabel)senderView.Superview.ViewWithTag(1);

                if(destinationContentVC != null)
                {
                    List<Campus> c = new List<Campus>();
                    c.Add(Campus.All);
                    if (TrafFilter)
                    {
                        c.Add(Campus.Trafalgar);
                    }
                    if(HMCFilter)
                    {
                        c.Add(Campus.HMC);
                    }
                    if(DavisFilter)
                    {
                        c.Add(Campus.Davis);
                    }
                    //retrieve the content group
                    ContentGroup groupToBeViewed = _repoRealmManager.GetContentGroup(labelText.Text);
                    //set the filter parameters
                    groupToBeViewed.SetFilterList(c);
                    //set the section group to the content group to be viewed
                    destinationContentVC.SectionContent = groupToBeViewed;
                    //set additonal UI features
                    destinationContentVC.SourceSegueID = segue.Identifier;
                    destinationContentVC.TitleText = labelText.Text;


					//Analytics code sourced from https://components.xamarin.com/gettingstarted/firebaseiosanalytics

					NSString[] keys = { ParameterNamesConstants.ContentType };
                    NSObject[] values = { new NSString(labelText.Text.Replace(" ", String.Empty)) };

					var parameters = NSDictionary<NSString, NSObject>.FromObjectsAndKeys(keys, values, keys.Length);

                    Analytics.LogEvent(EventNamesConstants.SelectContent, parameters);
                   
				}
            }

            //check to see if the user is looking for there favorites
            if(segue.Identifier == "FavoritesSegue")
            {
                var destinationContentVC = (ContentDetailViewController)segue.DestinationViewController;
                destinationContentVC.TitleText = "Favorites";
                destinationContentVC.SourceSegueID = segue.Identifier;
            }
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }


        [Action ("UnwindFromContentDetail:")]
        public void UnwindFromContentDetail(UIStoryboardSegue sender)
        {
        }

        [Action ("UnwindToThisVC:")]
        public void UnwindToThisVC(UIStoryboardSegue sender)
        {
            //handle unwinds to this VC
            //grab the source view controller
            var sourceVC = sender.SourceViewController;

            //check to see what the type is
            if(sourceVC.GetType() == typeof(SettingsViewController))
            {
                TrafFilter = _repoRealmManager.GetUser().TrafalgarPreference;
                DavisFilter = _repoRealmManager.GetUser().DavisPreference;
                HMCFilter = _repoRealmManager.GetUser().HMCPreference;
            }
        }

        //change the filter, and subsequnetly the scroll view
        //also add the white outline, suggesting which button is active
        partial void ThriveOnClick(UIButton sender)
        {
          
            ThriveScrollView.Hidden = false;
            SurviveScrollView.Hidden = true;
            //remove the survive button border
            btnSurvive.Layer.BorderWidth = 0;
            btnThrive.Layer.BorderColor = UIColor.Clear.CGColor;
            //add the thrive button border
            btnThrive.Layer.BorderWidth = 3;
            btnThrive.Layer.BorderColor = UIColor.White.CGColor;
        }

		//change the filter, and subsequnetly the scroll viewu
		//also add the white outline, suggesting which button is active
		partial void SurviveOnClick(UIButton sender)
        {
            ThriveScrollView.Hidden = true;
            SurviveScrollView.Hidden = false;
			//remove the survive button border
			btnThrive.Layer.BorderWidth = 0;
            btnThrive.Layer.BorderColor = UIColor.Clear.CGColor;
			//add the thrive button border
			btnSurvive.Layer.BorderWidth =3;
            btnSurvive.Layer.BorderColor = UIColor.White.CGColor;
        }

        partial void OnMenuItemClick(UIButton sender)
        {
            //When a menu item is clicked, intiate the segue
            PerformSegue("ContentDetailSegue", sender);
        }
    }
}

