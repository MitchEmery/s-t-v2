using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace SurviveAndThrive.Droid
{
    public class CampusFragment : Fragment
    {
        bool saveTralfargarPref;
        bool saveDavisPref;
        bool saveHMCPref;
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            
            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            // return inflater.Inflate(Resource.Layout.YourFragment, container, false);
            View view = inflater.Inflate(Resource.Layout.Campus, container, false);

            RealmManager _realmManager;
            _realmManager = new RealmManager();


            User pref = _realmManager.GetUser();

            Switch tralfagar =  view.FindViewById<Switch>(Resource.Id.switchTrafalgar);
            Switch davis = view.FindViewById<Switch>(Resource.Id.switchDavis);
            Switch hmc = view.FindViewById<Switch>(Resource.Id.switchHMC);
            Button credits = view.FindViewById<Button>(Resource.Id.Credits);
            if (savedInstanceState == null)
            {
                tralfagar.Checked = true;
                davis.Checked = true;
                hmc.Checked = true;

            }
            //else
            //{
                tralfagar.Checked = pref.TrafalgarPreference;
                davis.Checked = pref.DavisPreference;
                hmc.Checked = pref.HMCPreference;
            //}

            //tralfagar.Checked = pref.TrafalgarPreference;
            //davis.Checked = pref.DavisPreference;
            //hmc.Checked = pref.HMCPreference;

            //bool a = tralfagar.Checked = true;
            //bool b = davis.Checked = true;
            //bool c = hmc.Checked = true;



            tralfagar.CheckedChange += delegate (object sender, CompoundButton.CheckedChangeEventArgs e) {

                _realmManager.UpdateUserPrefs(tralfagar.Checked, hmc.Checked, davis.Checked);
            };


            davis.CheckedChange += delegate (object sender, CompoundButton.CheckedChangeEventArgs e) {

                _realmManager.UpdateUserPrefs(tralfagar.Checked, hmc.Checked, davis.Checked);
            };


            hmc.CheckedChange += delegate (object sender, CompoundButton.CheckedChangeEventArgs e) {

                _realmManager.UpdateUserPrefs(tralfagar.Checked, hmc.Checked, davis.Checked);
            };
            credits.Click += delegate
            {
                Intent intent = new Intent(Context, typeof(WebActivity));
                StartActivity(intent);
            };





            return view;
        }
    }
}