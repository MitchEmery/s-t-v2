using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace SurviveAndThrive.Droid
{
    public class ThriveFragment : Fragment
    {

        List<TextView> contentTitle = new List<TextView>();

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            // return inflater.Inflate(Resource.Layout.Thrive, container, false);
            View view = inflater.Inflate(Resource.Layout.Thrive, container, false);

            ImageButton academicbtn = view.FindViewById<ImageButton>(Resource.Id.imageButton1);
            ImageButton careerCenterbtn = view.FindViewById<ImageButton>(Resource.Id.imageButton2);
            ImageButton childCarebtn = view.FindViewById<ImageButton>(Resource.Id.imageButton3);
            ImageButton clubsbtn = view.FindViewById<ImageButton>(Resource.Id.imageButton4);
            ImageButton cutlurebtn = view.FindViewById<ImageButton>(Resource.Id.imageButton5);
            ImageButton gymbtn = view.FindViewById<ImageButton>(Resource.Id.imageButton6);
            ImageButton librarybtn = view.FindViewById<ImageButton>(Resource.Id.imageButton7);
            ImageButton securitybtn = view.FindViewById<ImageButton>(Resource.Id.imageButton8);
            ImageButton tipsbtn = view.FindViewById<ImageButton>(Resource.Id.imageButton9);
            ImageButton wellnessbtn = view.FindViewById<ImageButton>(Resource.Id.imageButton10);   
            ImageButton SSUbtn = view.FindViewById<ImageButton>(Resource.Id.imageButton11);
            ImageButton studySkillsbtn = view.FindViewById<ImageButton>(Resource.Id.imageButton12);

            contentTitle.Add(view.FindViewById<TextView>(Resource.Id.textView1));
            contentTitle.Add(view.FindViewById<TextView>(Resource.Id.textView2));
            contentTitle.Add(view.FindViewById<TextView>(Resource.Id.textView3));
            contentTitle.Add(view.FindViewById<TextView>(Resource.Id.textView4));
            contentTitle.Add(view.FindViewById<TextView>(Resource.Id.textView5));
            contentTitle.Add(view.FindViewById<TextView>(Resource.Id.textView6));
            contentTitle.Add(view.FindViewById<TextView>(Resource.Id.textView7));
            contentTitle.Add(view.FindViewById<TextView>(Resource.Id.textView8));
            contentTitle.Add(view.FindViewById<TextView>(Resource.Id.textView9));
            contentTitle.Add(view.FindViewById<TextView>(Resource.Id.textView10));
            contentTitle.Add(view.FindViewById<TextView>(Resource.Id.textView11));
            contentTitle.Add(view.FindViewById<TextView>(Resource.Id.textView12));
            academicbtn.Click += delegate
            {
                Intent intent = new Intent(this.Activity, typeof(ListActivity));
                intent.PutExtra("title", contentTitle[0].Text);    
                StartActivity(intent);
            };

            careerCenterbtn.Click += delegate
            {
                Intent intent = new Intent(this.Activity, typeof(ListActivity));
                intent.PutExtra("title", contentTitle[1].Text);
                StartActivity(intent);
            };

            childCarebtn.Click += delegate
            {
                Intent intent = new Intent(this.Activity, typeof(ListActivity));          
                intent.PutExtra("title", contentTitle[2].Text);
                StartActivity(intent);
            };

            clubsbtn.Click += delegate
            {
                Intent intent = new Intent(this.Activity, typeof(ListActivity));
                intent.PutExtra("title", contentTitle[3].Text);
                StartActivity(intent);
            };

            cutlurebtn.Click += delegate
            {
                Intent intent = new Intent(this.Activity, typeof(ListActivity));
                intent.PutExtra("title", contentTitle[4].Text);
                StartActivity(intent);
            };

            gymbtn.Click += delegate
            {
                Intent intent = new Intent(this.Activity, typeof(ListActivity));
                intent.PutExtra("title", contentTitle[5].Text);
                StartActivity(intent);
            };

            librarybtn.Click += delegate
            {
                Intent intent = new Intent(this.Activity, typeof(ListActivity));
                intent.PutExtra("title", contentTitle[6].Text);
                StartActivity(intent);
            };

            securitybtn.Click += delegate
            {
                Intent intent = new Intent(this.Activity, typeof(ListActivity));
                intent.PutExtra("title", contentTitle[7].Text);
                StartActivity(intent);
            };

            tipsbtn.Click += delegate
            {
                Intent intent = new Intent(this.Activity, typeof(ListActivity));
                intent.PutExtra("title", contentTitle[8].Text);
                StartActivity(intent);
            };

            wellnessbtn.Click += delegate
            {
                Intent intent = new Intent(this.Activity, typeof(ListActivity));
                intent.PutExtra("title", contentTitle[9].Text);
                StartActivity(intent);
            };

            SSUbtn.Click += delegate
            {
                Intent intent = new Intent(this.Activity, typeof(ListActivity));
                intent.PutExtra("title", contentTitle[10].Text);
                StartActivity(intent);
            };

            studySkillsbtn.Click += delegate
            {
                Intent intent = new Intent(this.Activity, typeof(ListActivity));
                intent.PutExtra("title", contentTitle[11].Text);
                StartActivity(intent);
            };

            return view;
            
        }
    }
}