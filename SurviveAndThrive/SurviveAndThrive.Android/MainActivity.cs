﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Com.Balysv.Material.Drawable.Menu;
using Android.Graphics;
using static Android.Views.View;
using System.Collections.Generic;
using Java.Lang;

namespace SurviveAndThrive.Droid
{
    [Activity(Label = "SurviveAndThrive.Android", MainLauncher = false, ConfigurationChanges = Android.Content.PM.ConfigChanges.ScreenSize | Android.Content.PM.ConfigChanges.Orientation),]
    public class MainActivity : Activity, IOnClickListener
    {



        // needed to create a custom toolbar and its actions
        Toolbar toolbar;
        int actionBarMenuState;
        RealmManager _realmManager;
        User pref;
        bool saveTralfargarPref;
        bool saveDavisPref;
        bool saveHMCPref;
        private bool backpressed =false;
        //VListView listfragment = new VListView();
        enum Stroke
        {
            REGULAR = 3,
            THIN = 2,
            EXTRA_THIN = 1
        }

        Button survive;
        protected override void OnCreate(Bundle bundle)
        {
            //_realmManager = new RealmManager();
            //pref = _realmManager.GetUser();
            base.OnCreate(bundle);
            //
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);
            // Get our button from the layout resource,
            // and attach an event to it





            survive = FindViewById<Button>(Resource.Id.buttonSurvive);
            Button thrive = FindViewById<Button>(Resource.Id.buttonThrive);
            survive.SetBackgroundResource(Resource.Layout.capsuleBtnPressed);
            // Create a new fragment and a transaction.
            FragmentTransaction fragmentTx = FragmentManager.BeginTransaction();

            SurviveFragment sfragment = new SurviveFragment();
            ThriveFragment tfragment = new ThriveFragment();

            //Switch trafalgarSwitch = FindViewById<Switch>(Resource.Id.switchTrafalgar);
            //Switch davisSwitch = FindViewById<Switch>(Resource.Id.switchDavis);
            //Switch HMCSwitch = FindViewById<Switch>(Resource.Id.switchHMC);


            //if (trafalgarSwitch != null)
            //    { trafalgarSwitch.CheckedChange += delegate (object sender, CompoundButton.CheckedChangeEventArgs e) {
            //        Toast.MakeText(this, "Your answer is " + (e.IsChecked ? "correct" : "incorrect"), ToastLength.Short).Show();
            //     };
            //}



            // The fragment will have the ID of Resource.Id.fragment_container.
            fragmentTx.Add(Resource.Id.fragment_container, sfragment);

            // Commit the transaction.
            fragmentTx.Commit();

            survive.Click += delegate
            {

                FragmentTransaction fragmentTxSwitch = this.FragmentManager.BeginTransaction();
                fragmentTxSwitch.Replace(Resource.Id.fragment_container, sfragment);

                // Add the transaction to the back stack.
                //fragmentTxSwitch.AddToBackStack(null);
                // Commit the transaction.
                survive.SetBackgroundResource(Resource.Layout.capsuleBtnPressed);
                thrive.SetBackgroundResource(Resource.Layout.capsuleBtn);
                fragmentTxSwitch.SetTransition(FragmentTransit.FragmentFade);
                fragmentTxSwitch.Commit();

                //Toast.MakeText(this, "Survive pressed ", ToastLength.Short).Show();
            };

            thrive.Click += delegate
            {


                FragmentTransaction fragmentTxSwitch = this.FragmentManager.BeginTransaction();
                fragmentTxSwitch.Replace(Resource.Id.fragment_container, tfragment);
                // Add the transaction to the back stack.
                //fragmentTxSwitch.AddToBackStack(null);
                // Commit the transaction.

                thrive.SetBackgroundResource(Resource.Layout.capsuleBtnPressed);
                survive.SetBackgroundResource(Resource.Layout.capsuleBtn);
                fragmentTxSwitch.SetTransition(FragmentTransit.FragmentFade);
                fragmentTxSwitch.Commit();


                //ActionBar.Title = "THRIVE";
                //Toast.MakeText(this, "Thrive pressed ", ToastLength.Short).Show();
            };

            toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            SetActionBar(toolbar);
            ActionBar.Title = "";


            MaterialMenuDrawable materialMenu = new MaterialMenuDrawable(this, Color.White, (int)Stroke.EXTRA_THIN,
                MaterialMenuDrawable.DefaultScale,
                MaterialMenuDrawable.DefaultTransformDuration);
            toolbar.NavigationIcon = materialMenu;
            toolbar.SetNavigationOnClickListener(this);
            {
            };

        }
        // adding the search and favourite buttons to menu 
        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.top_menus, menu);
            return base.OnCreateOptionsMenu(menu);
        }
        // handles search and favourite button actions
        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            //Toast.MakeText(this,item.TitleFormatted,ToastLength.Short).Show();
            string menuCheck = "" + item.TitleFormatted;
            //Toast.MakeText(this, menuCheck, ToastLength.Short).Show();
            if (menuCheck.Equals("Search"))
            {
                Intent intent = new Intent(this, typeof(SearchActivity));
                intent.PutExtra("title", "Search");
                StartActivity(intent);
            }
            else if (menuCheck.Equals("Favourite"))
            {
                Intent intent = new Intent(this, typeof(ListActivity));
                intent.PutExtra("title", "Favourite");
                StartActivity(intent);
            }


            return base.OnOptionsItemSelected(item);
        }

        // this adds the burger menu button.
        public void OnClick(View v)
        {
            //actionBarMenuState = GenerateState(actionBarMenuState);
            
            GetMaterialMenu(toolbar).AnimateIconState(IntToState(actionBarMenuState), false);
        }

        // handles the burger menu and back button actions when going to settings/campus
        private MaterialMenuDrawable.IconState IntToState(int currentactionBarMenuState)
        {
            FragmentTransaction fragmentTxCampus;
            if (currentactionBarMenuState == 0)
            {
                backpressed = false;
                // show campus menu page
                actionBarMenuState = 1;
                fragmentTxCampus = this.FragmentManager.BeginTransaction();
                CampusFragment cfragment = new CampusFragment();
                fragmentTxCampus.Replace(Resource.Id.fragment_container2, cfragment);
                ActionBar.Title = "Settings";
                fragmentTxCampus.SetTransition(FragmentTransit.FragmentFade);
                //fragmentTxCampus.AddToBackStack(null);
                // Commit the transaction.
                fragmentTxCampus.Commit();
                return MaterialMenuDrawable.IconState.Arrow;
            }
            else if (currentactionBarMenuState == 1 || backpressed == true)
            {
                // hide menu campus page
                actionBarMenuState = 0;
                fragmentTxCampus = this.FragmentManager.BeginTransaction();
                BlankFragment blank = new BlankFragment();
                fragmentTxCampus.Replace(Resource.Id.fragment_container2, blank);
                ActionBar.Title = "";

                fragmentTxCampus.SetTransition(FragmentTransit.FragmentFade);
                // Commit the transaction.
                fragmentTxCampus.Commit();
                return MaterialMenuDrawable.IconState.Burger;
            }
            else
            {
                return MaterialMenuDrawable.IconState.Burger;
            }

        }

        private MaterialMenuDrawable GetMaterialMenu(Toolbar toolbar)
        {
            return (MaterialMenuDrawable)toolbar.NavigationIcon;
        }

        // handles the need when you press back in the campus fragment or the main page in main Activity.
        public override void OnBackPressed()
        {
            if (actionBarMenuState == 1)
            {
                FragmentTransaction fragmentTxCampus;
                // hide menu campus page
                actionBarMenuState = 0;
                fragmentTxCampus = this.FragmentManager.BeginTransaction();
                BlankFragment blank = new BlankFragment();
                fragmentTxCampus.Replace(Resource.Id.fragment_container2, blank);
                ActionBar.Title = "";

                fragmentTxCampus.SetTransition(FragmentTransit.FragmentFade);
                // Commit the transaction.
                fragmentTxCampus.Commit();
                GetMaterialMenu(toolbar).AnimateIconState(MaterialMenuDrawable.IconState.Burger, false);
                //base.OnBackPressed();
                backpressed = true;
            }
            else if (actionBarMenuState == 0)
            {
                base.OnBackPressed();
            }
        }

    }

}


