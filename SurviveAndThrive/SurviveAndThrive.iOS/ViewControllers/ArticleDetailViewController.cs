﻿using System;
using Foundation;
using UIKit;
using System.Threading;
using CoreGraphics;
using SafariServices;
using Firebase.Analytics;

namespace SurviveAndThrive.iOS
{
    public partial class ArticleDetailViewController : UIViewController
    {
        public Article ArticleForDisplay { get; set; }
        private nfloat _fontSize = 17f;
        NSError error = null;

        public ArticleDetailViewController(IntPtr handle) : base(handle)
        { }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.
            ArticleForDisplay.StartChallenge();
            //set the text scroller to show the article text
            _txtScroller.AttributedText = new NSAttributedString(ArticleForDisplay.Text, new NSAttributedStringDocumentAttributes { DocumentType = NSDocumentType.HTML }, ref error); 
            //grab the youtube video embed link and set the webviewer to display it
            _webViewer.LoadRequest(new NSUrlRequest(new NSUrl(ArticleForDisplay.ArticleVideo.VideoEmbedURL)));
			//set the label to the title of the article
			_lblTitle.Text = ArticleForDisplay.Title;
            //make sure the text view is the right size
            _txtScroller.SizeToFit();
            _txtScroller.Font = UIFont.FromName("Helvetica", _fontSize);
            //ovveride default URL handling
            _txtScroller.AllowUrlInteraction = (textView, url, characterRange, interaction) => 
            {

                SFSafariViewController sfvc = new SFSafariViewController(url);
                //sfvc.PreferredControlTintColor = new UIColor(0, 0.695f, 0.804f, 1.0f);
                sfvc.PreferredBarTintColor = new UIColor(0, 0.214f, 0.402f, 1.0f);
                this.PresentViewController(sfvc, true, null);

                return false;
            };


            NSString[] keys = { ParameterNamesConstants.ItemName };
            NSObject[] values = { new NSString(ArticleForDisplay.Title.Replace(" ", String.Empty))};

			var parameters = NSDictionary<NSString, NSObject>.FromObjectsAndKeys(keys, values, keys.Length);
            Analytics.LogEvent(EventNamesConstants.ViewItem , parameters);

        }


        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }
   
        //on the closing of the view end the challenge
        public override void ViewWillDisappear(bool animated)
        {
			//if the challenge is complete, the timer is already disposed
			if (!ArticleForDisplay.IsComplete)
			{
				//dispose the timer
				ArticleForDisplay.EndChallenge();
			}
        }

        partial void OnUpFont(NSObject sender)
        {
            if (_fontSize < 40f )
            {
                _txtScroller.Font = UIFont.FromName("Helvetica", _fontSize += 5f);
                //re size the text view to accomadate the new size of the view
                _txtScroller.SizeToFit();
            }
        }

        partial void OnDownFont(NSObject sender)
        {
            if (_fontSize > 12f )
            {
                _txtScroller.Font = UIFont.FromName("Helvetica", _fontSize -= 5f);
                //resize the textview to accomodate for the new size of the view
                _txtScroller.SizeToFit();
            }
        }
    }
}

