﻿using System;
using System.Collections.Generic;
using Foundation;
using UIKit;
using Realms;
using System.Linq;
using System.CodeDom.Compiler;

namespace SurviveAndThrive.iOS
{
    public partial class ContentDetailViewController : UIViewController
    {
        private TableDataSource _articleTableDataSource;
        private bool _isFavorites = false;

        public string SourceSegueID { get; set; }

        public string TitleText { get; set; }
        public ContentGroup SectionContent { get; set; }

        private RealmManager _rmanager;

        public ContentDetailViewController(IntPtr handle) : base(handle)
        {
            //intialize the realm manager
            _rmanager = new RealmManager();
        }

        public override void ViewDidAppear(bool animated)
        {
            if (SectionContent != null)
            {
                if (SectionContent.getFilteredList().Count == 0)
                {
                    AlertUserOfNoContent();
                }
            }
            else
            {
                if(_rmanager.RetrieveFavourites().Count == 0)
                {
                    AlertUserOfNoFavorites();
                }
            }
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.

            //if the user is coming from the favorites button press, 
            //prepare to show the favorites 
            if (SourceSegueID.Equals("FavoritesSegue"))
            {
                //disable the favorites button
                _btnFavorites.Enabled = false;
				//get the favorites from the realm
                _articleTableDataSource = new TableDataSource(_rmanager.RetrieveFavourites());
            }
            //otherwise, get the requested content group from the content group passed from the 
            //home page, and filter the list accordingly
            else
            {
				_articleTableDataSource = new TableDataSource(SectionContent.getFilteredList());	  
            }
            //set the data for the list view
            _articleTable.Source = _articleTableDataSource;
            //Set the title for the page
			lblContentTitle.Text = TitleText;
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }

        /// <summary>
        /// Back button press, also checks for favorites
        /// </summary>
        /// <param name="sender">Sender.</param>
        partial void OnBackButtonPress(Foundation.NSObject sender)
        {
            BackHandler();
        }

        /// <summary>
        /// Show the user the favorites, but dont leave the page
        /// </summary>
        /// <param name="sender">Sender.</param>
        partial void OnFavoritesClick(Foundation.NSObject sender)
        {
            //the user cant look at favorites, they already are
            _btnFavorites.Enabled = false;
            //update the data to reflect the change
            _articleTableDataSource.updateSearchList(_rmanager.RetrieveFavourites());
            _articleTable.ReloadData();
            _articleTable.ReloadInputViews();
			//global var for tracking weather the user is looking at favorites
			_isFavorites = true;

            if (_rmanager.RetrieveFavourites().Count == 0)
            {
                AlertUserOfNoFavorites();
            }


        }

        /// <summary>
        /// Prepares for segue.
        /// </summary>
        /// <param name="segue">Segue.</param>
        /// <param name="sender">Sender.</param>
        public override void PrepareForSegue(UIStoryboardSegue segue, Foundation.NSObject sender)
        {
            base.PrepareForSegue(segue, sender);
            if(segue.Identifier == "ArticleDetailSegue")
            {
                var destinationVC = (ArticleDetailViewController)segue.DestinationViewController;
                var cell = (UITableViewCell)sender;
                //pass the article the user slected
                destinationVC.ArticleForDisplay = _articleTableDataSource.getCurrentDisplayedList()[(int)_articleTable.IndexPathForSelectedRow.LongRow];
            }


        }

		[Action("UnwindFromArticleDetail:")]
		public void UnwindFromArticleDetail(UIStoryboardSegue sender)
		{

		}

        private void AlertUserOfNoContent()
        {
            //create a new view controller to handle the alert
            var alert = UIAlertController.Create("No Content Yet!", "More Content Coming Soon!", UIAlertControllerStyle.Alert);
            //add an aclert to the VC
            alert.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default,(obj) => { PerformSegue("UnwindSegue", this);}));
			if (alert.PopoverPresentationController != null)
				alert.PopoverPresentationController.SourceView = this.View;
            //present it
            this.PresentViewController(alert, animated: true, completionHandler: null);
        }

        private void AlertUserOfNoFavorites()
        {
			//create a new view controller to handle the alert
			var alert = UIAlertController.Create("No Favorites Yet!", "Click The Star Next To An Item In The List To Favorite It!", UIAlertControllerStyle.Alert);
            //add an aclert to the VC
            alert.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, (obj) =>
            {
                BackHandler();
            }));

			if (alert.PopoverPresentationController != null)
				alert.PopoverPresentationController.SourceView = this.View;
			//present it
			this.PresentViewController(alert, animated: true, completionHandler: null); 
        }


        private void BackHandler()
        {
			if (!_isFavorites)
			{
				PerformSegue("UnwindSegue", this);
			}
			//if the user is looking, chnage the data but dont leave the page
			else
			{
				//the user can now look at favorites
				_btnFavorites.Enabled = true;
				//reload data to refelect change in content
				_articleTableDataSource.updateSearchList(SectionContent.getFilteredList());
				_articleTable.ReloadData();
				//global var for tracking weather the user is looking at favorites
				_isFavorites = false;
			}
        }
		[Action("UnwindFromCreditsDetail:")]
		public void UnwindFromCreditsDetail(UIStoryboardSegue sender)
		{
		}
    }

}

