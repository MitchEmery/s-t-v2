﻿using Realms;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Google.Apis.YouTube.v3;
using Google.Apis.Services;
using Google.Apis.YouTube.v3.Data;

namespace SurviveAndThrive
{
    
    public enum SurviveOrThrive : byte
    {
        Survive = 1,
        Thrive = 2
    }
    [Preserve(AllMembers = true)]
    public class ContentGroup : RealmObject
    {
        //Repersents a group of articles
        //The List Of articles 
        public IList<Article> ContentArticles { get; }
        //The title of this section
        public string Title { get; set; }
        //The content type is used to connect articles and their content groups
        public string ContentType { get; set; }
        //Survive Or Thrive
        private byte _surviveFlag { get; set; }
        //the filters
        private List<Campus> CampusFilterList;

        /// <summary>
        /// Takes a title and the content type as a parameter,
        /// intializes the list
        /// </summary>
        /// <param name="title"></param>
        /// <param name="contentType"></param>
        public ContentGroup(string title, string contentType, SurviveOrThrive flag)
        {
            Title = title;
            ContentType = contentType;
            _surviveFlag = (byte)flag;
            ContentArticles = new List<Article>();
        }

        //Contrusctor with no parameters needed for Realm
        public ContentGroup()
        { }

        //accessor/mutator for the survive flag
        public void setSurviveFlag(SurviveOrThrive s)
        {
            _surviveFlag = (byte)s;
        }

        public SurviveOrThrive getSurviveFlag()
        {
            return (SurviveOrThrive)_surviveFlag;
        }

        /// <summary>
        /// Gets the filtered list based on the campus filters.
        /// </summary>
        /// <returns>The filtered list.</returns>
        public List<Article> getFilteredList()
        {

            //match the list of campus filters to the current article list
            List<Article> articlesToBeSeen = ContentArticles.Where(x => CampusFilterList.Contains(x.getCampusFilter())).ToList();

            List<string> videoDurations = new List<string>();

			var youtubeSvc = new YouTubeService(new BaseClientService.Initializer()
			{
				ApiKey = "AIzaSyAWwpEwU6D856pSU-XgLNGxa3bJgVWXuPE",
				ApplicationName = this.GetType().ToString()
			});

            try
            {
				Dictionary<string, string> parameters = new Dictionary<string, string>();

				parameters.Add("part", "contentDetails");

                string CSVids = "";

                foreach(Article a in articlesToBeSeen)
                {
                    if(CSVids != "")
                    {
                        CSVids += "," + a.ArticleVideo.VideoID;
                    }
                    else
                    {
                        CSVids += a.ArticleVideo.VideoID;
                    }
                }
				
                parameters.Add("id", CSVids);

				VideosResource.ListRequest videosListByID = youtubeSvc.Videos.List(parameters["part"]);

				if (parameters.ContainsKey("part") && parameters["id"] != "")
				{
					videosListByID.Id = parameters["id"];
				}

				VideoListResponse response = videosListByID.ExecuteAsync().Result;

                foreach(Video v in response.Items)
                {
                    string duration = v.ContentDetails.Duration.Substring(2);
                    string VideoDuration = "";
                    if (duration != "")
                    {
                        for (int i = 0; i < duration.Length; i++)
                        {
                            if (char.IsDigit(duration[i]))
                            {
                                VideoDuration += duration[i];
                            }
                            else
                            {
                                if (duration[i] != 'S')
                                {
                                    VideoDuration += ':';

                                    if (!Char.IsDigit(duration[i + 2]))
                                    {
                                        VideoDuration += '0';
                                    }
                                }
                            }
                        }


                        if (!VideoDuration.Contains(":"))
                        {
                            VideoDuration = "0:" + VideoDuration;
                        }
                    } 
                    videoDurations.Add(VideoDuration);
                }
			}
            catch(Exception e)
            {
                
            }
            if (videoDurations.Count == articlesToBeSeen.Count)
            {


                this.Realm.Write(() =>
                {

                    for (int i = 0; i < articlesToBeSeen.Count; i++)
                    {
                        articlesToBeSeen[i].ArticleVideo.VideoDuration = videoDurations[i];
                    }

                });
            }
			return articlesToBeSeen;
        }

        /// <summary>
        /// Set's the filter parameters 
        /// </summary>
        /// <param name="c">a list of enum elements that set the campus filters</param>
        public void SetFilterList(List<Campus> c)
        {
            CampusFilterList = c;
        }

        public override int GetHashCode()
        {
            return (_surviveFlag.GetHashCode() ^ (Title ?? String.Empty).GetHashCode() ^ (ContentType ?? String.Empty).GetHashCode());
        }

        public override bool Equals(object obj)
        {
            if (obj == null) { return false; }

            ContentGroup compareeCG = obj as ContentGroup;

            return _surviveFlag == compareeCG._surviveFlag &&
                                             Title == compareeCG.Title &&
                                             ContentType == compareeCG.ContentType;
        }
    }
}
