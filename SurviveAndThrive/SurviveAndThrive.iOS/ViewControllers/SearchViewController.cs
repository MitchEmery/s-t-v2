﻿using System;
using System.Collections.Generic;
using UIKit;
using System.CodeDom.Compiler;
using Foundation;


namespace SurviveAndThrive.iOS
{
    public partial class SearchViewController : UIViewController
    {
        //The realm manager
        private RealmManager _rManager;
        //the data source, handling content for the list view
        private TableDataSource _searchResultsDataSource;

        public SearchViewController(IntPtr handle) : base(handle)
        {
            //Start with an Empty list of articles
            _searchResultsDataSource = new TableDataSource(new List<Article>());
            //intialize the realm manager
            _rManager = new RealmManager();
        }

        /// <summary>
        /// Using the Realm Manager class, 
        /// querys for articles containing typed values
        /// and updates the tabledata source
        /// </summary>
        /// <param name="sender">Sender</param>
        partial void OnSearchBarChange(Foundation.NSObject sender)
        {
            //check for empty search field
            if(_txtFieldSearchBar.Text != String.Empty)
            {
                //search for articles
                List<Article> results = _rManager.SearchArtices(_txtFieldSearchBar.Text);
                //if that length of the results is 0, report it to the user
                if(results.Count != 0)
                {
                    //Update the search results by searching using the realm manager
                    _searchResultsDataSource.updateSearchList(results);
                    //reload the data to reflect the changes to the user
                    _tblSearchResults.ReloadData();
                }
                else
                {
                    //create a new view controller to handle the alert
                    var alert = UIAlertController.Create("No Content Matches Your Search.", "More Content Coming Soon!", UIAlertControllerStyle.Alert);
                    //add an aclert to the VC
                    alert.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, (obj) => { _txtFieldSearchBar.Text = ""; }));
                    if (alert.PopoverPresentationController != null)
                        alert.PopoverPresentationController.SourceView = this.View;
                    //present it
                    this.PresentViewController(alert, animated: true, completionHandler: null);
                }

            }
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.
            //set the table data source to the source property of the table
            _tblSearchResults.Source = _searchResultsDataSource;
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }

        /// <summary>
        /// Prepares for segue to Article Detail Page,
        /// from the search setting
        /// </summary>
        /// <param name="segue">Segue.</param>
        /// <param name="sender">Sender.</param>
        public override void PrepareForSegue(UIStoryboardSegue segue, Foundation.NSObject sender)
        {
            //call the base 
            base.PrepareForSegue(segue, sender);
            //Check to confirm the segue
            if (segue.Identifier == "ArticleDetailSegue")
            {
                //set the article specified to the one selected
                //type cast the destination view controller
                var destinationVC = (ArticleDetailViewController)segue.DestinationViewController;
                //type case the sender to a cell
                var cell = (UITableViewCell)sender;
                //grab the specfied article and pass it to the destination view controller
                destinationVC.ArticleForDisplay = _searchResultsDataSource.getCurrentDisplayedList()[(int)_tblSearchResults.IndexPathForSelectedRow.LongRow];
            }


        }

        /// <summary>
        /// Unwinds from article detail.
        /// </summary>
        /// <param name="sender">Sender.</param>
        [Action("UnwindFromArticleDetail:")]
        public void UnwindFromArticleDetail(UIStoryboardSegue sender)
        { }
    }
}

