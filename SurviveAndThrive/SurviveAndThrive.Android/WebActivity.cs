﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Webkit;

namespace SurviveAndThrive.Droid
{
    [Activity(Label = "WebActivity")]
    public class WebActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Web);
            // Create your application here

            WebView page = FindViewById<WebView>(Resource.Id.Page);

            page.LoadUrl("file:///android_asset/Credits.html");
            //page.
        }
    }
}