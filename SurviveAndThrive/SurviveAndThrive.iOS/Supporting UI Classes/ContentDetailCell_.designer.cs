// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace SurviveAndThrive.iOS
{
    [Register ("ContentDetailCell")]
    partial class ContentDetailCell
    {
        [Outlet]
        public UIKit.UIButton _btnFavorites { get; private set; }

        [Outlet]
        public UIKit.UIImageView _imgThumbnail { get; private set; }

        [Outlet]
        public UIKit.UILabel _lblArticleDesc { get; private set; }

        [Outlet]
        public UIKit.UILabel _lblcampus { get; set; }

        [Outlet]
        public UIKit.UILabel _lblTime { get; private set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        public UIKit.UILabel _lblTitle { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (_btnFavorites != null) {
                _btnFavorites.Dispose ();
                _btnFavorites = null;
            }

            if (_imgThumbnail != null) {
                _imgThumbnail.Dispose ();
                _imgThumbnail = null;
            }

            if (_lblArticleDesc != null) {
                _lblArticleDesc.Dispose ();
                _lblArticleDesc = null;
            }

            if (_lblcampus != null) {
                _lblcampus.Dispose ();
                _lblcampus = null;
            }

            if (_lblTime != null) {
                _lblTime.Dispose ();
                _lblTime = null;
            }

            if (_lblTitle != null) {
                _lblTitle.Dispose ();
                _lblTitle = null;
            }
        }
    }
}