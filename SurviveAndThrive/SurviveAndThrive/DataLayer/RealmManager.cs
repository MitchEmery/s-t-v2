﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using Realms;


namespace SurviveAndThrive
{
    public class RealmManager
    {
        //The configuration for the realm stipulates behavior in changes in realm structure
        private RealmConfiguration _config;
        //We need two realms, for multiple threads
        //primary realm for handling UI and other TOP level threads
        public Realm _primaryRealm;
        //secondary threads for System.Threading.Timers timer related operations
        public Realm _secondaryRealm;

        public RealmManager()
        {
            //the configuration set up, for development and likely
            //prod we will wipe the realm and start from scratch
            //maybe not for the user obj
            _config = new RealmConfiguration
            {
                ShouldDeleteIfMigrationNeeded = true
            };
            //set up the intial realm for main thread ops
            _primaryRealm = Realm.GetInstance(_config);
        }

        /// <summary>
        /// Querys the Realm for all articles that
        /// have the IsFavorite marked true
        /// </summary>
        /// <returns>returns all articles that are marked favourite.</returns>
        public List<Article> RetrieveFavourites()
        {
            //return the articles from the realm where IsFavorite is true
           return _primaryRealm.All<Article>().Where(x => x.IsFavorite).ToList();
        }

        /// <summary>
        /// querys the realm for articles that have the keyword in the 
        /// bulk of the article or in the title
        /// </summary>
        /// <returns>A list of articles that match the search parameters</returns>
        /// <param name="keyword">Search word</param>
        public List<Article> SearchArtices(string keyword)
        {
			//query the realm
			var results =
                from art in _primaryRealm.All<Article>()
				where (art.Title.Contains(keyword, StringComparison.OrdinalIgnoreCase) || art.Text.Contains(keyword, StringComparison.OrdinalIgnoreCase))
				select art;
            
			//return the search results
			List<Article> searchResults = results.ToList();
			return searchResults;
        }



		/// <summary>
		/// Update the user name
		/// </summary>
		/// <param name="newUserName">New user name.</param>
		public void SetUserName(string newUserName)
		{
			//grab the sole user
			User user = _primaryRealm.Find<User>("UserIDOne");

			//the find operator returns one result or null
			//check to see if user isnt null
			if (user != null)
			{
				//update the users preferences about campus
				user.Realm.Write(() =>
				{
					user.UserName = newUserName;
				});
			}
		}

		/// <summary>
		/// Updates the user prefs.
		/// </summary>
		/// <param name="traf">If set to <c>true</c> traf pref set </param>
		/// <param name="hmc">If set to <c>true</c> hmc pref set </param>
		/// <param name="davis">If set to <c>true</c> davis pref set </param>
		public void UpdateUserPrefs(bool traf, bool hmc, bool davis)
		{
			//grab the sole user
			User user = _primaryRealm.Find<User>("UserIDOne");

			//the find operator returns one result or null
			//check to see if user isnt null
			if (user != null)
			{
				//update the users preferences about campus
				user.Realm.Write(() =>
				{
					user.TrafalgarPreference = traf;
					user.HMCPreference = hmc;
					user.DavisPreference = davis;
				});
			}
		}

		/// <summary>
		/// Gets the user.
		/// </summary>
		/// <returns>The user.</returns>
		public User GetUser()
		{
			//return the user
			return _primaryRealm.Find<User>("UserIDOne");
		}

        /// <summary>
        /// Gets the completion percentage.
        /// </summary>
        /// <returns>The completion percentage.</returns>
        public float GetCompletionPercentage()
        {
            int completedNumber = _primaryRealm.All<Article>().Where(x => x.IsComplete).ToList().Count;
            int totalNumber = _primaryRealm.All<Article>().Count();

            if (completedNumber != 0)
            {
                return completedNumber / totalNumber;
            }
            else
                return 0;


        }
     
    }
}
