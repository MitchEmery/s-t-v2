using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Webkit;
using Android.Text;
using Android.Content.Res;
using System.IO;
using Android.Webkit;
using Newtonsoft.Json;
using System.Globalization;
using Java.Text;
using Java.Util;
using Android.Text.Method;

namespace SurviveAndThrive.Droid
{
    public class DetailFragment : Fragment
    {
        Bundle bundle;
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetHasOptionsMenu(true);
            // Create your fragment here

        }

        // changing the list of menu options
        public override void OnCreateOptionsMenu(IMenu menu, MenuInflater inflater)
        {
            menu.Clear();
            bundle = new Bundle();
            bool isSearched = Arguments.GetBoolean("isSearched");
            if (isSearched)
                inflater.Inflate(Resource.Menu.size_only, menu);
            else
                inflater.Inflate(Resource.Menu.size_menu, menu);
            // TODO Add your menu entries here
            //return 
            base.OnCreateOptionsMenu(menu, inflater);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            View view = inflater.Inflate(Resource.Layout.Details, container, false);
            Bundle bundle = new Bundle();

            //string saved = bundle.GetString("article2");
            string saved = Arguments.GetString("article");

            WebView video = view.FindViewById<WebView>(Resource.Id.Video);
            WebView links = view.FindViewById<WebView>(Resource.Id.Links);
            //WebView weblink = view.FindViewById<WebView>(Resource.Id.Info);

            TextView time = view.FindViewById<TextView>(Resource.Id.Time);
            TextView title = view.FindViewById<TextView>(Resource.Id.Title);
            TextView info = view.FindViewById<TextView>(Resource.Id.Info);
            AssetManager assets = Android.App.Application.Context.Assets;
            Article currentArticle;
            currentArticle = JsonConvert.DeserializeObject<Article>(saved);
            links.Visibility = ViewStates.Invisible;
            links.Settings.JavaScriptEnabled = true;
            links.SetWebViewClient(new WebViewClient());

            video.Settings.JavaScriptEnabled = true;
            video.SetWebChromeClient(new WebChromeClient());
            video.LoadUrl(currentArticle.ArticleVideo.VideoEmbedURL);

            //info.SetWebChromeClient(new WebChromeClient());
            //info.Settings.JavaScriptEnabled = true;
            //https://www.sheridancollege.ca/admissions/fees-and-finances/academic-fees/2016-2017-academic-fees/administration-fees-and-charges.aspx
            //info.LoadUrl("infoLibrary.html");
            //string text2 = "<a href=\"http://www.google.ca \">This is a link</a>";
            //string testHtml = "<p>&bull; Check out the <a href= \"http://www.google.ca \" >breakdown of fees</a> </p><p>&bull; Check 'View My Fee Invoice' in myStudent Center under 'Finances' for a breakdown of the fees you owe</p><p>&bull; Pay your fees online through your bank, by mail or in person</p><p>&emsp;&omicron; Allow plenty of time for money to arrive at school if not paying in person</p><p>&emsp;&omicron; Pay in-person using debit, cheque, or money order at the Office of the Registrar</p><p>&emsp;&omicron; Mail a certified cheque or money order payable to Sheridan College</p><p>&emsp;&omicron; Avoid the $150 late fee by staying on top of deadlines</p><p>&emsp;&omicron; If Sheridan doesn't work out for you, be aware that the $500 administrative fee is nonrefundable</p><p>&bull; Apply for bursaries/awards on myStudent Centre</p><p>&emsp;&omicron; Fill out your 'Online Award Application' profile to find appropriate scholarships</p><p>&bull; Apply for OSAP if you are an Ontario resident and require financial aid to automatically submit for the 30% off Ontario Tuition Grant</p><p>&bull; Visit your provinces' scholarship and bursary website if you�re an out of province student. Googling your home province and then financial aid is usually an easy way to find it</p><p>&bull; Find your T2202A form under 'Other Financial' on myStudent Center</p>";

            string htmlText;
            htmlText = currentArticle.Text;
            //using (StreamReader htmlReader = new StreamReader(assets.Open("infoLibrary.html")))
            //{
            //    htmlText = htmlReader.ReadToEnd();
            //    info.SetText(Html.FromHtml(htmlText), TextView.BufferType.Spannable);
            //}
            links.LoadData(htmlText, "text/html", null);
            links.Visibility = ViewStates.Visible;
            //info.SetText(Html.FromHtml(htmlText), TextView.BufferType.Spannable);
            //info.MovementMethod = LinkMovementMethod.Instance;
            time.Text = currentArticle.ArticleVideo.VideoDuration;




            /*ISpanned msg2 = Html.FromHtml("<h2>Title</h2><br><p>Description here</p>");
            info.Text = Html.from("<h2>Title</h2><br><p>Description here</p>");

            favs.Click += delegate
            {
                favs.SetImageResource(Resource.Drawable.ic_menu_favourite_customgold2);
            };

            info.SetTextSize(TypedValue.COMPLEX_UNIT_SP, 50);
            */


            return view;
        }

        static public Article FromJSON<Article>(string code)
        {
            var item = JsonConvert.DeserializeObject<Article>(code);
            return item;
        }

        //public class MyWebViewClient : WebViewClient
        //{
        //    Boolean loadingFinished = true;
        //    Boolean redirect = false;
        //    public override bool ShouldOverrideUrlLoading(WebView views, IWebResourceRequest request)
        //    {
        //        views.SetMinimumHeight(200);
        //        views.LoadUrl(request.Url.ToString());
        //        return false;
        //    }


        //    public override bool ShouldOverrideUrlLoading(WebView view, string url)
        //    {
        //        //if (!loadingFinished)
        //        //{
        //        //    redirect = true;
        //        //}

        //        //loadingFinished = false;
        //        view.LoadUrl(url);
        //        Console.WriteLine("Loading web view...");
        //        return false;
        //    }

        //    public override void OnPageStarted(WebView view, string url, Android.Graphics.Bitmap favicon)
        //    {
        //        base.OnPageStarted(view, url, favicon);
        //        loadingFinished = false;
        //    }

        //    public override void OnPageFinished(WebView view, string url)
        //    {
        //        if (!redirect)
        //        {
        //            loadingFinished = true;
        //        }

        //        if (loadingFinished && !redirect)
        //        {
        //            Console.WriteLine("Finished Loading!!!");
        //        }
        //        else
        //        {
        //            redirect = false;
        //        }
        //    }
        //}

    }

}